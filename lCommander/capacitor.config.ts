import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'lcommander.iesvirgendelcarmen.es',
  appName: 'lCommander',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
