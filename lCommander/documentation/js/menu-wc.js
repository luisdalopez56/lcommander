'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">lCommander documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AddEmpleadoPageModule.html" data-type="entity-link" >AddEmpleadoPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AddEmpleadoPageModule-08c3fdf148552b9b176460bd44f800b9dc41f0de8091fb4f6688293b9fd58e660d785361411700e82889c01f442ee432c5a9c507b3c44b6bdeb096e50b9168e7"' : 'data-target="#xs-components-links-module-AddEmpleadoPageModule-08c3fdf148552b9b176460bd44f800b9dc41f0de8091fb4f6688293b9fd58e660d785361411700e82889c01f442ee432c5a9c507b3c44b6bdeb096e50b9168e7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AddEmpleadoPageModule-08c3fdf148552b9b176460bd44f800b9dc41f0de8091fb4f6688293b9fd58e660d785361411700e82889c01f442ee432c5a9c507b3c44b6bdeb096e50b9168e7"' :
                                            'id="xs-components-links-module-AddEmpleadoPageModule-08c3fdf148552b9b176460bd44f800b9dc41f0de8091fb4f6688293b9fd58e660d785361411700e82889c01f442ee432c5a9c507b3c44b6bdeb096e50b9168e7"' }>
                                            <li class="link">
                                                <a href="components/AddEmpleadoPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddEmpleadoPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AddEmpleadoPageRoutingModule.html" data-type="entity-link" >AddEmpleadoPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AddPlatoPageModule.html" data-type="entity-link" >AddPlatoPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AddPlatoPageModule-8698335e72e765d53c1259cd14094cfab00750e2363989431c5593f454950e5862d8f862deb5537e5817e4cb95922c55ea04aef5570689799ec09f1777b3fa35"' : 'data-target="#xs-components-links-module-AddPlatoPageModule-8698335e72e765d53c1259cd14094cfab00750e2363989431c5593f454950e5862d8f862deb5537e5817e4cb95922c55ea04aef5570689799ec09f1777b3fa35"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AddPlatoPageModule-8698335e72e765d53c1259cd14094cfab00750e2363989431c5593f454950e5862d8f862deb5537e5817e4cb95922c55ea04aef5570689799ec09f1777b3fa35"' :
                                            'id="xs-components-links-module-AddPlatoPageModule-8698335e72e765d53c1259cd14094cfab00750e2363989431c5593f454950e5862d8f862deb5537e5817e4cb95922c55ea04aef5570689799ec09f1777b3fa35"' }>
                                            <li class="link">
                                                <a href="components/AddPlatoPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddPlatoPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AddPlatoPageRoutingModule.html" data-type="entity-link" >AddPlatoPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-9d5c634d6ad2ab85455e204433857916e9315d9f1768f2b6379efa9067397dc666378d0cb776cbbe9bf6c437a0d81b5244003b51da328eba786f15e46fe907cc"' : 'data-target="#xs-components-links-module-AppModule-9d5c634d6ad2ab85455e204433857916e9315d9f1768f2b6379efa9067397dc666378d0cb776cbbe9bf6c437a0d81b5244003b51da328eba786f15e46fe907cc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-9d5c634d6ad2ab85455e204433857916e9315d9f1768f2b6379efa9067397dc666378d0cb776cbbe9bf6c437a0d81b5244003b51da328eba786f15e46fe907cc"' :
                                            'id="xs-components-links-module-AppModule-9d5c634d6ad2ab85455e204433857916e9315d9f1768f2b6379efa9067397dc666378d0cb776cbbe9bf6c437a0d81b5244003b51da328eba786f15e46fe907cc"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CamarerosPageModule.html" data-type="entity-link" >CamarerosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CamarerosPageModule-d7e7c843bb8724b8282baa03a87b8c7a50c06216eae8a95f02bebd3ff6d22d6d21888359a0005f63b173885d2f4b7bba6a01af04c76b70e76d3af1e1d320603e"' : 'data-target="#xs-components-links-module-CamarerosPageModule-d7e7c843bb8724b8282baa03a87b8c7a50c06216eae8a95f02bebd3ff6d22d6d21888359a0005f63b173885d2f4b7bba6a01af04c76b70e76d3af1e1d320603e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CamarerosPageModule-d7e7c843bb8724b8282baa03a87b8c7a50c06216eae8a95f02bebd3ff6d22d6d21888359a0005f63b173885d2f4b7bba6a01af04c76b70e76d3af1e1d320603e"' :
                                            'id="xs-components-links-module-CamarerosPageModule-d7e7c843bb8724b8282baa03a87b8c7a50c06216eae8a95f02bebd3ff6d22d6d21888359a0005f63b173885d2f4b7bba6a01af04c76b70e76d3af1e1d320603e"' }>
                                            <li class="link">
                                                <a href="components/CamarerosPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CamarerosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CamarerosPageRoutingModule.html" data-type="entity-link" >CamarerosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CartaPageModule.html" data-type="entity-link" >CartaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CartaPageModule-255543dc0389558cb4435c5f1295c5c974cac871d974f584ae6a9f25b2e1b538c170109aa1cf41ace7317b0ee61f6bbe73d9fa4f9c042187f5ebbcbebab77e5d"' : 'data-target="#xs-components-links-module-CartaPageModule-255543dc0389558cb4435c5f1295c5c974cac871d974f584ae6a9f25b2e1b538c170109aa1cf41ace7317b0ee61f6bbe73d9fa4f9c042187f5ebbcbebab77e5d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CartaPageModule-255543dc0389558cb4435c5f1295c5c974cac871d974f584ae6a9f25b2e1b538c170109aa1cf41ace7317b0ee61f6bbe73d9fa4f9c042187f5ebbcbebab77e5d"' :
                                            'id="xs-components-links-module-CartaPageModule-255543dc0389558cb4435c5f1295c5c974cac871d974f584ae6a9f25b2e1b538c170109aa1cf41ace7317b0ee61f6bbe73d9fa4f9c042187f5ebbcbebab77e5d"' }>
                                            <li class="link">
                                                <a href="components/CartaPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CartaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CartaPageRoutingModule.html" data-type="entity-link" >CartaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ClientesPageModule.html" data-type="entity-link" >ClientesPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ClientesPageModule-47ce9c5d883020f7c40bd6041e9895c26bbf5565065a8e738083016d2320009c80b3818ce858bccffc328162a773cecbbdbde711477d7c7e2be810503fd85009"' : 'data-target="#xs-components-links-module-ClientesPageModule-47ce9c5d883020f7c40bd6041e9895c26bbf5565065a8e738083016d2320009c80b3818ce858bccffc328162a773cecbbdbde711477d7c7e2be810503fd85009"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ClientesPageModule-47ce9c5d883020f7c40bd6041e9895c26bbf5565065a8e738083016d2320009c80b3818ce858bccffc328162a773cecbbdbde711477d7c7e2be810503fd85009"' :
                                            'id="xs-components-links-module-ClientesPageModule-47ce9c5d883020f7c40bd6041e9895c26bbf5565065a8e738083016d2320009c80b3818ce858bccffc328162a773cecbbdbde711477d7c7e2be810503fd85009"' }>
                                            <li class="link">
                                                <a href="components/ClientesPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ClientesPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ClientesPageRoutingModule.html" data-type="entity-link" >ClientesPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CocinaPageModule.html" data-type="entity-link" >CocinaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CocinaPageModule-da0fc07c387b6094cb63f9e3abac060144b3dc380d3a98e074910ce2a892797783c9bf3ec2706ed3857e1f43e6bc0ec771ab01918e8cc9c66c63e67cc60eb99c"' : 'data-target="#xs-components-links-module-CocinaPageModule-da0fc07c387b6094cb63f9e3abac060144b3dc380d3a98e074910ce2a892797783c9bf3ec2706ed3857e1f43e6bc0ec771ab01918e8cc9c66c63e67cc60eb99c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CocinaPageModule-da0fc07c387b6094cb63f9e3abac060144b3dc380d3a98e074910ce2a892797783c9bf3ec2706ed3857e1f43e6bc0ec771ab01918e8cc9c66c63e67cc60eb99c"' :
                                            'id="xs-components-links-module-CocinaPageModule-da0fc07c387b6094cb63f9e3abac060144b3dc380d3a98e074910ce2a892797783c9bf3ec2706ed3857e1f43e6bc0ec771ab01918e8cc9c66c63e67cc60eb99c"' }>
                                            <li class="link">
                                                <a href="components/CocinaPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CocinaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CocinaPageRoutingModule.html" data-type="entity-link" >CocinaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ComandasPageModule.html" data-type="entity-link" >ComandasPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ComandasPageModule-2f3fb4ef86be7468c3aca73c77f437a22858a02d4029a6bcf874800e9bf60c406aab288cbd119abcf4b44c4cdd691d47fc27eda74ec65548e2dd99a9ce3a4a06"' : 'data-target="#xs-components-links-module-ComandasPageModule-2f3fb4ef86be7468c3aca73c77f437a22858a02d4029a6bcf874800e9bf60c406aab288cbd119abcf4b44c4cdd691d47fc27eda74ec65548e2dd99a9ce3a4a06"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ComandasPageModule-2f3fb4ef86be7468c3aca73c77f437a22858a02d4029a6bcf874800e9bf60c406aab288cbd119abcf4b44c4cdd691d47fc27eda74ec65548e2dd99a9ce3a4a06"' :
                                            'id="xs-components-links-module-ComandasPageModule-2f3fb4ef86be7468c3aca73c77f437a22858a02d4029a6bcf874800e9bf60c406aab288cbd119abcf4b44c4cdd691d47fc27eda74ec65548e2dd99a9ce3a4a06"' }>
                                            <li class="link">
                                                <a href="components/ComandasPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ComandasPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ComandasPageRoutingModule.html" data-type="entity-link" >ComandasPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardPageModule.html" data-type="entity-link" >DashboardPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DashboardPageModule-60130cbe3b6e6a5857244cdf4c871b5766dfdf7666444d92718bb4358a56ae93a1399f1fe299f8e27b2e9e916186f884ff329956262a1428659113462a793feb"' : 'data-target="#xs-components-links-module-DashboardPageModule-60130cbe3b6e6a5857244cdf4c871b5766dfdf7666444d92718bb4358a56ae93a1399f1fe299f8e27b2e9e916186f884ff329956262a1428659113462a793feb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardPageModule-60130cbe3b6e6a5857244cdf4c871b5766dfdf7666444d92718bb4358a56ae93a1399f1fe299f8e27b2e9e916186f884ff329956262a1428659113462a793feb"' :
                                            'id="xs-components-links-module-DashboardPageModule-60130cbe3b6e6a5857244cdf4c871b5766dfdf7666444d92718bb4358a56ae93a1399f1fe299f8e27b2e9e916186f884ff329956262a1428659113462a793feb"' }>
                                            <li class="link">
                                                <a href="components/DashboardPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DashboardPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardPageRoutingModule.html" data-type="entity-link" >DashboardPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DetalleMesaPageModule.html" data-type="entity-link" >DetalleMesaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DetalleMesaPageModule-e5abbee7386ca28b67d331dc57413ea16b75cc010c72d4fb1ff98c1f15642509457340c6b7b21bba305d0053fbc67786010ca00b8709a27339f2cb1ac1c3a45f"' : 'data-target="#xs-components-links-module-DetalleMesaPageModule-e5abbee7386ca28b67d331dc57413ea16b75cc010c72d4fb1ff98c1f15642509457340c6b7b21bba305d0053fbc67786010ca00b8709a27339f2cb1ac1c3a45f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DetalleMesaPageModule-e5abbee7386ca28b67d331dc57413ea16b75cc010c72d4fb1ff98c1f15642509457340c6b7b21bba305d0053fbc67786010ca00b8709a27339f2cb1ac1c3a45f"' :
                                            'id="xs-components-links-module-DetalleMesaPageModule-e5abbee7386ca28b67d331dc57413ea16b75cc010c72d4fb1ff98c1f15642509457340c6b7b21bba305d0053fbc67786010ca00b8709a27339f2cb1ac1c3a45f"' }>
                                            <li class="link">
                                                <a href="components/DetalleMesaPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DetalleMesaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DetalleMesaPageRoutingModule.html" data-type="entity-link" >DetalleMesaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/EmpleadosPageModule.html" data-type="entity-link" >EmpleadosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EmpleadosPageModule-744e2089770db4b92475f3b00896434119a19524ca1f862bd1e28537ca6be33859bdababc1709a05637e8c3b0db4dfb968ee4c1f08ae4da160a755d1fc232d19"' : 'data-target="#xs-components-links-module-EmpleadosPageModule-744e2089770db4b92475f3b00896434119a19524ca1f862bd1e28537ca6be33859bdababc1709a05637e8c3b0db4dfb968ee4c1f08ae4da160a755d1fc232d19"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EmpleadosPageModule-744e2089770db4b92475f3b00896434119a19524ca1f862bd1e28537ca6be33859bdababc1709a05637e8c3b0db4dfb968ee4c1f08ae4da160a755d1fc232d19"' :
                                            'id="xs-components-links-module-EmpleadosPageModule-744e2089770db4b92475f3b00896434119a19524ca1f862bd1e28537ca6be33859bdababc1709a05637e8c3b0db4dfb968ee4c1f08ae4da160a755d1fc232d19"' }>
                                            <li class="link">
                                                <a href="components/EmpleadosPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EmpleadosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EmpleadosPageRoutingModule.html" data-type="entity-link" >EmpleadosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LoginPageModule.html" data-type="entity-link" >LoginPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginPageModule-9bb69787f6957ed9aba05e9dd379419c499488d340b4edbd3f540c00f0843798a4d0e04eb2e89c86651546765b2790940a675d29866f94929255c0dc1de8fbee"' : 'data-target="#xs-components-links-module-LoginPageModule-9bb69787f6957ed9aba05e9dd379419c499488d340b4edbd3f540c00f0843798a4d0e04eb2e89c86651546765b2790940a675d29866f94929255c0dc1de8fbee"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginPageModule-9bb69787f6957ed9aba05e9dd379419c499488d340b4edbd3f540c00f0843798a4d0e04eb2e89c86651546765b2790940a675d29866f94929255c0dc1de8fbee"' :
                                            'id="xs-components-links-module-LoginPageModule-9bb69787f6957ed9aba05e9dd379419c499488d340b4edbd3f540c00f0843798a4d0e04eb2e89c86651546765b2790940a675d29866f94929255c0dc1de8fbee"' }>
                                            <li class="link">
                                                <a href="components/LoginPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginPageRoutingModule.html" data-type="entity-link" >LoginPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MesasPageModule.html" data-type="entity-link" >MesasPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MesasPageModule-e71b4dea49d75e557a07d7eef74e28d2f81a6ee475589e0d3a5df10f4a428aac9003c69c60705e2db6ef7ac96b906ed2df1fc2c05305cc3201381de233ef4e60"' : 'data-target="#xs-components-links-module-MesasPageModule-e71b4dea49d75e557a07d7eef74e28d2f81a6ee475589e0d3a5df10f4a428aac9003c69c60705e2db6ef7ac96b906ed2df1fc2c05305cc3201381de233ef4e60"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MesasPageModule-e71b4dea49d75e557a07d7eef74e28d2f81a6ee475589e0d3a5df10f4a428aac9003c69c60705e2db6ef7ac96b906ed2df1fc2c05305cc3201381de233ef4e60"' :
                                            'id="xs-components-links-module-MesasPageModule-e71b4dea49d75e557a07d7eef74e28d2f81a6ee475589e0d3a5df10f4a428aac9003c69c60705e2db6ef7ac96b906ed2df1fc2c05305cc3201381de233ef4e60"' }>
                                            <li class="link">
                                                <a href="components/MesasPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MesasPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MesasPageRoutingModule.html" data-type="entity-link" >MesasPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PagarConTarjetaPageModule.html" data-type="entity-link" >PagarConTarjetaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PagarConTarjetaPageModule-0562c82e2be5254d4f5e48c6c6b43321e85b96d3a8f24b22a698e96f5cbd6aba1e2626e30c35324a7a419e7bef32a5c2210d72a6fd280c3e3ed41ab7e0bb980b"' : 'data-target="#xs-components-links-module-PagarConTarjetaPageModule-0562c82e2be5254d4f5e48c6c6b43321e85b96d3a8f24b22a698e96f5cbd6aba1e2626e30c35324a7a419e7bef32a5c2210d72a6fd280c3e3ed41ab7e0bb980b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PagarConTarjetaPageModule-0562c82e2be5254d4f5e48c6c6b43321e85b96d3a8f24b22a698e96f5cbd6aba1e2626e30c35324a7a419e7bef32a5c2210d72a6fd280c3e3ed41ab7e0bb980b"' :
                                            'id="xs-components-links-module-PagarConTarjetaPageModule-0562c82e2be5254d4f5e48c6c6b43321e85b96d3a8f24b22a698e96f5cbd6aba1e2626e30c35324a7a419e7bef32a5c2210d72a6fd280c3e3ed41ab7e0bb980b"' }>
                                            <li class="link">
                                                <a href="components/PagarConTarjetaPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PagarConTarjetaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PagarConTarjetaPageRoutingModule.html" data-type="entity-link" >PagarConTarjetaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PedirPlatosPageModule.html" data-type="entity-link" >PedirPlatosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PedirPlatosPageModule-12403bc709dbefb3850a7acb1bb8a2747ada2b9a91f93b7a4109e3b3534ca8cf9d4d4eb002185a16686114cfab80da8f0c82e8893dde22cdab1c1a14c11b6f2c"' : 'data-target="#xs-components-links-module-PedirPlatosPageModule-12403bc709dbefb3850a7acb1bb8a2747ada2b9a91f93b7a4109e3b3534ca8cf9d4d4eb002185a16686114cfab80da8f0c82e8893dde22cdab1c1a14c11b6f2c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PedirPlatosPageModule-12403bc709dbefb3850a7acb1bb8a2747ada2b9a91f93b7a4109e3b3534ca8cf9d4d4eb002185a16686114cfab80da8f0c82e8893dde22cdab1c1a14c11b6f2c"' :
                                            'id="xs-components-links-module-PedirPlatosPageModule-12403bc709dbefb3850a7acb1bb8a2747ada2b9a91f93b7a4109e3b3534ca8cf9d4d4eb002185a16686114cfab80da8f0c82e8893dde22cdab1c1a14c11b6f2c"' }>
                                            <li class="link">
                                                <a href="components/PedirPlatosPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PedirPlatosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PedirPlatosPageRoutingModule.html" data-type="entity-link" >PedirPlatosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PopoverAlergenosPageModule.html" data-type="entity-link" >PopoverAlergenosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PopoverAlergenosPageModule-b207559236d6ecd69833bc1005c52878e7f9edcda5d140e7149bffb44e57a9a285474ddc957d9a55ccd901454b22b53fda19c65c9135f5d9e3bab23f57f4c3f6"' : 'data-target="#xs-components-links-module-PopoverAlergenosPageModule-b207559236d6ecd69833bc1005c52878e7f9edcda5d140e7149bffb44e57a9a285474ddc957d9a55ccd901454b22b53fda19c65c9135f5d9e3bab23f57f4c3f6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PopoverAlergenosPageModule-b207559236d6ecd69833bc1005c52878e7f9edcda5d140e7149bffb44e57a9a285474ddc957d9a55ccd901454b22b53fda19c65c9135f5d9e3bab23f57f4c3f6"' :
                                            'id="xs-components-links-module-PopoverAlergenosPageModule-b207559236d6ecd69833bc1005c52878e7f9edcda5d140e7149bffb44e57a9a285474ddc957d9a55ccd901454b22b53fda19c65c9135f5d9e3bab23f57f4c3f6"' }>
                                            <li class="link">
                                                <a href="components/PopoverAlergenosPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PopoverAlergenosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PopoverAlergenosPageRoutingModule.html" data-type="entity-link" >PopoverAlergenosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PopoverCambiarImagenPageModule.html" data-type="entity-link" >PopoverCambiarImagenPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PopoverCambiarImagenPageModule-d5f96ec827b5b4ad7d1ec23779ffebb31fba811a80110bc9df740b56a36b9d6209ba2daaed1843a57cc4eef86dee5bde672361c2cb3ae1b040ed5bd6b3c6a80b"' : 'data-target="#xs-components-links-module-PopoverCambiarImagenPageModule-d5f96ec827b5b4ad7d1ec23779ffebb31fba811a80110bc9df740b56a36b9d6209ba2daaed1843a57cc4eef86dee5bde672361c2cb3ae1b040ed5bd6b3c6a80b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PopoverCambiarImagenPageModule-d5f96ec827b5b4ad7d1ec23779ffebb31fba811a80110bc9df740b56a36b9d6209ba2daaed1843a57cc4eef86dee5bde672361c2cb3ae1b040ed5bd6b3c6a80b"' :
                                            'id="xs-components-links-module-PopoverCambiarImagenPageModule-d5f96ec827b5b4ad7d1ec23779ffebb31fba811a80110bc9df740b56a36b9d6209ba2daaed1843a57cc4eef86dee5bde672361c2cb3ae1b040ed5bd6b3c6a80b"' }>
                                            <li class="link">
                                                <a href="components/PopoverCambiarImagenPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PopoverCambiarImagenPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PopoverCambiarImagenPageRoutingModule.html" data-type="entity-link" >PopoverCambiarImagenPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RealizarCobroPageModule.html" data-type="entity-link" >RealizarCobroPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RealizarCobroPageModule-d7eb5d79250e71667eaab8f66883012ae8d6c1018f2ac84dd62db4412a63db5635cef50570074146f4dcea02a13ef2fbaef106c758a998a7270d403f85e56896"' : 'data-target="#xs-components-links-module-RealizarCobroPageModule-d7eb5d79250e71667eaab8f66883012ae8d6c1018f2ac84dd62db4412a63db5635cef50570074146f4dcea02a13ef2fbaef106c758a998a7270d403f85e56896"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RealizarCobroPageModule-d7eb5d79250e71667eaab8f66883012ae8d6c1018f2ac84dd62db4412a63db5635cef50570074146f4dcea02a13ef2fbaef106c758a998a7270d403f85e56896"' :
                                            'id="xs-components-links-module-RealizarCobroPageModule-d7eb5d79250e71667eaab8f66883012ae8d6c1018f2ac84dd62db4412a63db5635cef50570074146f4dcea02a13ef2fbaef106c758a998a7270d403f85e56896"' }>
                                            <li class="link">
                                                <a href="components/RealizarCobroPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RealizarCobroPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RealizarCobroPageRoutingModule.html" data-type="entity-link" >RealizarCobroPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RegistrarEmpresaPageModule.html" data-type="entity-link" >RegistrarEmpresaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RegistrarEmpresaPageModule-737d7fa3d0b5480c2cff1ff516ffad31f260299f443a044e6e67f14b1d717da5970d56386c8a4af9ec3063b537352a1f4917e87954f503ba2df2353cd107fb72"' : 'data-target="#xs-components-links-module-RegistrarEmpresaPageModule-737d7fa3d0b5480c2cff1ff516ffad31f260299f443a044e6e67f14b1d717da5970d56386c8a4af9ec3063b537352a1f4917e87954f503ba2df2353cd107fb72"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RegistrarEmpresaPageModule-737d7fa3d0b5480c2cff1ff516ffad31f260299f443a044e6e67f14b1d717da5970d56386c8a4af9ec3063b537352a1f4917e87954f503ba2df2353cd107fb72"' :
                                            'id="xs-components-links-module-RegistrarEmpresaPageModule-737d7fa3d0b5480c2cff1ff516ffad31f260299f443a044e6e67f14b1d717da5970d56386c8a4af9ec3063b537352a1f4917e87954f503ba2df2353cd107fb72"' }>
                                            <li class="link">
                                                <a href="components/RegistrarEmpresaPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegistrarEmpresaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RegistrarEmpresaPageRoutingModule.html" data-type="entity-link" >RegistrarEmpresaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TicketsPageModule.html" data-type="entity-link" >TicketsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TicketsPageModule-578fc69c39a31a5da5f5309442ba9a1afa0cc6af26c01cc33e458b99942962db3c1a054485dd98a3abafcb6506886090cff67f9827e1c10cd31a84894ad85ca2"' : 'data-target="#xs-components-links-module-TicketsPageModule-578fc69c39a31a5da5f5309442ba9a1afa0cc6af26c01cc33e458b99942962db3c1a054485dd98a3abafcb6506886090cff67f9827e1c10cd31a84894ad85ca2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TicketsPageModule-578fc69c39a31a5da5f5309442ba9a1afa0cc6af26c01cc33e458b99942962db3c1a054485dd98a3abafcb6506886090cff67f9827e1c10cd31a84894ad85ca2"' :
                                            'id="xs-components-links-module-TicketsPageModule-578fc69c39a31a5da5f5309442ba9a1afa0cc6af26c01cc33e458b99942962db3c1a054485dd98a3abafcb6506886090cff67f9827e1c10cd31a84894ad85ca2"' }>
                                            <li class="link">
                                                <a href="components/TicketsPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TicketsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TicketsPageRoutingModule.html" data-type="entity-link" >TicketsPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/VerQRPageModule.html" data-type="entity-link" >VerQRPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-VerQRPageModule-e9189e40fcffdac393b270499dafc9a039e3611ae9c854ff336bb08acfbcbf5ab52140092d32cab18786937ffdb707b6e920fa34233b5558cdb0cfe99c1d3887"' : 'data-target="#xs-components-links-module-VerQRPageModule-e9189e40fcffdac393b270499dafc9a039e3611ae9c854ff336bb08acfbcbf5ab52140092d32cab18786937ffdb707b6e920fa34233b5558cdb0cfe99c1d3887"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-VerQRPageModule-e9189e40fcffdac393b270499dafc9a039e3611ae9c854ff336bb08acfbcbf5ab52140092d32cab18786937ffdb707b6e920fa34233b5558cdb0cfe99c1d3887"' :
                                            'id="xs-components-links-module-VerQRPageModule-e9189e40fcffdac393b270499dafc9a039e3611ae9c854ff336bb08acfbcbf5ab52140092d32cab18786937ffdb707b6e920fa34233b5558cdb0cfe99c1d3887"' }>
                                            <li class="link">
                                                <a href="components/VerQRPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VerQRPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/VerQRPageRoutingModule.html" data-type="entity-link" >VerQRPageRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/CategoriaPlatos.html" data-type="entity-link" >CategoriaPlatos</a>
                            </li>
                            <li class="link">
                                <a href="classes/Comanda.html" data-type="entity-link" >Comanda</a>
                            </li>
                            <li class="link">
                                <a href="classes/Departamento.html" data-type="entity-link" >Departamento</a>
                            </li>
                            <li class="link">
                                <a href="classes/Empresa.html" data-type="entity-link" >Empresa</a>
                            </li>
                            <li class="link">
                                <a href="classes/Estancias.html" data-type="entity-link" >Estancias</a>
                            </li>
                            <li class="link">
                                <a href="classes/Mesa.html" data-type="entity-link" >Mesa</a>
                            </li>
                            <li class="link">
                                <a href="classes/Plato.html" data-type="entity-link" >Plato</a>
                            </li>
                            <li class="link">
                                <a href="classes/Ticket.html" data-type="entity-link" >Ticket</a>
                            </li>
                            <li class="link">
                                <a href="classes/Usuario.html" data-type="entity-link" >Usuario</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ControlInformacionService.html" data-type="entity-link" >ControlInformacionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FirebaseAuthService.html" data-type="entity-link" >FirebaseAuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FirebaseFirestoreService.html" data-type="entity-link" >FirebaseFirestoreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FirebaseStorageService.html" data-type="entity-link" >FirebaseStorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StripeService.html" data-type="entity-link" >StripeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/VariablesService.html" data-type="entity-link" >VariablesService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuardService.html" data-type="entity-link" >AuthGuardService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/PlatoComanda.html" data-type="entity-link" >PlatoComanda</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});