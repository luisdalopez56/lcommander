// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBavkJcDug2WPWajE80eguK6DBdp6nxWpU",
    authDomain: "lcommander-203bf.firebaseapp.com",
    databaseURL: "https://lcommander-203bf-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "lcommander-203bf",
    storageBucket: "lcommander-203bf.appspot.com",
    messagingSenderId: "900585027835",
    appId: "1:900585027835:web:8459ce1ef6908146a1585e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
