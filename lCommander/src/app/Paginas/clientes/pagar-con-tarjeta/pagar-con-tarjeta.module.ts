import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagarConTarjetaPageRoutingModule } from './pagar-con-tarjeta-routing.module';

import { PagarConTarjetaPage } from './pagar-con-tarjeta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagarConTarjetaPageRoutingModule
  ],
  declarations: [PagarConTarjetaPage]
})
export class PagarConTarjetaPageModule {}
