import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagarConTarjetaPage } from './pagar-con-tarjeta.page';

const routes: Routes = [
  {
    path: '',
    component: PagarConTarjetaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagarConTarjetaPageRoutingModule {}
