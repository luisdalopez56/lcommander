import { ControlInformacionService } from './../../../Servicios/control-informacion.service';
import { NavParams, Platform, ModalController } from '@ionic/angular';
import { StripeService } from './../../../Servicios/stripe.service';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-pagar-con-tarjeta',
  templateUrl: './pagar-con-tarjeta.page.html',
  styleUrls: ['./pagar-con-tarjeta.page.scss'],
})
export class PagarConTarjetaPage implements OnInit {
  pagoValido: boolean;
  pagoCorrecto: boolean = false;
  constructor(
    public ngZone: NgZone,
    public navParams: NavParams,
    public stripeService: StripeService,
    public platform: Platform,
    public modalCtrl: ModalController,
    public controlInformacion: ControlInformacionService
  ) {
  }

  @ViewChild('cardInfo') cadInfo: ElementRef;
  cardError: string;
  card: any;

  cuentaTotal: number;

  ngOnInit() {
    this.cuentaTotal = this.navParams.data.cuentaTotal;
    console.log(this.cuentaTotal);

  }

  ionViewWillLeave() {
    this.card.destroy()
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }

  ngAfterViewInit(): void {
    this.card = elements.create('card');
    this.card.mount(this.cadInfo.nativeElement);
    this.card.addEventListener('change', this.onChange.bind(this));
  }

  onChange(ev) {
    if (ev.complete == false) this.pagoValido = false
    else if (ev.complete == true && ev.error) this.pagoValido = false
    else this.pagoValido = true

    if (ev.error?.message) this.ngZone.run(() => {
      this.cardError = ev.error.message;
    })
    else this.ngZone.run(() => {
      this.cardError = null;
    })
  }

  async pedirToken() {
    await this.controlInformacion.presentLoading("Realizando pago...");
    const { token, error } = await stripe.createToken(this.card);
    if (token) {
      const response = await this.stripeService.charge(this.cuentaTotal, token.id);
      let resStripe = response as any
      if (resStripe.status == "succeeded") {
        this.controlInformacion.mostrarToast("Pago correcto", "success");
        this.pagoCorrecto = true;
        this.controlInformacion.dismissLoading();
        setTimeout(() => {
          this.modalCtrl.dismiss(true);
        }, 3000);
      } else {
        this.controlInformacion.mostrarToast("Error al realizar el pago", "danger");
        this.controlInformacion.dismissLoading();
      }

    } else {
      this.ngZone.run(() => {
        this.cardError = null;
      })
    }
  }
}
