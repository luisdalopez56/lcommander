import { Usuario } from './../../Modelos/Usuario';
import { FirebaseFCMService } from 'src/app/Servicios/Firebase/firebase-fcm.service';
import { PagarConTarjetaPage } from './pagar-con-tarjeta/pagar-con-tarjeta.page';
import { AlertController, ModalController } from '@ionic/angular';
import { Empresa } from 'src/app/Modelos/Empresa';
import { Mesa } from 'src/app/Modelos/Mesa';
import { Estancias } from 'src/app/Modelos/Estancias';
import { FirebaseFirestoreService } from 'src/app/Servicios/Firebase/firebase-firestore.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';
import { Ticket } from 'src/app/Modelos/Ticket';
import * as moment from 'moment';
import { CategoriaPlatos } from 'src/app/Modelos/CategoriaPlatos';
import { Plato } from 'src/app/Modelos/Plato';
import { Comanda, PlatoComanda } from 'src/app/Modelos/Comanda';
import { VariablesService } from 'src/app/Servicios/variables.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {
  carta: CategoriaPlatos[];
  mesaActual: Mesa;
  empresaActual: Empresa = null;
  estanciaActual: Estancias = null;
  tickets: Ticket[] = [];
  mostrarDetalleCuenta: boolean = false;
  pidiendoPlatos: boolean = false;
  platoClickado: string;
  comandaActual: Comanda;
  comandas: Comanda[] = [];
  ticketMesa: Ticket;

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public controlInformacion: ControlInformacionService,
    public firestoreService: FirebaseFirestoreService,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public ngzone: NgZone,
    public variables: VariablesService,
    public firebaseMessaging: FirebaseFCMService
  ) { }


  ngOnInit() {
    this.comprobarParametros()
  }

  async addPlatoEspecial(plato: Plato) {
    let alert = await this.alertCtrl.create({
      header: "Plato especial",
      message: "Añadir instrucciones",
      inputs: [{
        type: "textarea",
        placeholder: "Añade instrucciones",
        label: "observaciones",
        name: "observaciones"
      }],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Añadir a comanda",
          handler: async (datos) => {
            let platoComanda: PlatoComanda = { id: plato.id, precio: plato.precio, estado: 0, nombrePlato: plato.nombrePlato, observaciones: datos.observaciones };
            this.comandaActual.platos.push(platoComanda);
          }

        }
      ]
    })

    alert.present();
  }



  comprobarParametros() {
    this.activatedRoute.queryParams
      .subscribe(async params => {
        console.log(params);

        if (params.guidEmpresa && params.idMesa) {
          this.empresaActual = { NombreEmpresa: "", NIFEmpresa: params.guidEmpresa };
          this.variables.accesoCliente = true;
          (await this.firestoreService.getEstanciasRestaurante(this.empresaActual)).subscribe((estancias) => {
            console.log("Estancias: ", estancias);

            let estanciaActual = estancias as any;
            estanciaActual.forEach(estancia => {
              if (estancia.mesas.filter(mesaFiltrada => mesaFiltrada.id == params.idMesa).length > 0) {
                this.mesaActual = estancia.mesas.filter(mesaFiltrada => mesaFiltrada.id == params.idMesa)[0];
                this.estanciaActual = estancia;
              }
            })

          })

          await (await this.firestoreService.getEmpleadosEmpresa(this.empresaActual)).subscribe((usuarios) => {
            this.variables.empleadosEmpresa = usuarios as Usuario[];
          })

          await (await this.firestoreService.getComandasRestaurante(this.empresaActual)).subscribe((comandas) => {
            this.comandas = comandas as Comanda[];
          })

          await (await this.firestoreService.getTicketsRestaurante(this.empresaActual)).subscribe((tickets) => {
            this.tickets = tickets as Ticket[];
            this.ticketMesa = this.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesaActual.id && ticketFiltrado.estado != 2)[0];
            console.log(this.ticketMesa);

          })

          await (await this.firestoreService.getCartaRestaurante(this.empresaActual)).subscribe((carta) => {
            this.carta = carta as CategoriaPlatos[];
          })



        } else {
          this.controlInformacion.mostrarToast("Por favor escanea el código QR de la mesa para hacer el pedido", "danger")
          this.router.navigate(['']);
        }
        console.log(params); // { orderby: "price" }

      }
      );
  }

  clickarPlato(plato: Plato) {

    if (this.platoClickado != plato.id) {
      this.platoClickado = plato.id;
    } else {
      this.platoClickado = null;
    }

  }

  cantidadEnComanda(plato: any): number {
    let cantidad: number;
    if (plato.observaciones) {
      cantidad = this.comandaActual.platos.filter(platoFiltrado => platoFiltrado.id == plato.id && platoFiltrado.observaciones == plato.observaciones).length;
    } else {
      cantidad = this.comandaActual.platos.filter(platoFiltrado => platoFiltrado.id == plato.id && !platoFiltrado.observaciones).length;
    }

    return cantidad;
  }

  addPlatoComanda(plato: Plato, observaciones?: string) {
    let platoComanda: PlatoComanda = { id: plato.id, precio: plato.precio, estado: 0, nombrePlato: plato.nombrePlato };
    this.comandaActual.platos.push(platoComanda);
  }

  deletePlatoComanda(plato: Plato) {
    let findIndex = this.comandaActual.platos.findIndex(platoFiltrado => platoFiltrado.id == plato.id);
    this.comandaActual.platos.splice(findIndex, 1);
  }

  cuentaTotal(): number {
    let cuenta = 0;
    this.comandaActual.platos.forEach(plato => {
      cuenta = cuenta + plato.precio;
    })

    return cuenta;
  }

  comandasPendientes(): Comanda[] {
    let comandasPendientesActualizado = [];
    let comandasPendientes = this.comandas.filter(comandaFiltro => comandaFiltro.estado == 0 && comandaFiltro.mesa.id == this.mesaActual.id);
    comandasPendientes.forEach((comanda) => {
      comanda.platos = comanda.platos.filter(platoFiltrado => platoFiltrado.estado < 3);
      comandasPendientesActualizado.push(comanda);
    })
    return comandasPendientesActualizado
  }

  async finalizarServicio() {
    let alert = await this.alertCtrl.create({
      header: "Método de pago",
      message: "Selecciona el método de pago que vas a utilizar",
      buttons: [
        {
          text: "EFECTIVO",
          handler: () => {

            this.ticketMesa.estado = 1;
            this.ticketMesa.precioTotal = Number.parseFloat(this.sumaTicketActual());
            this.firestoreService.addTicket(this.empresaActual, this.ticketMesa)
              .then(() => {
                console.log("Ticket actualizado");
                this.controlInformacion.mostrarToast("Espera al camarero o ve a la barra para hacer el pago");
                let empleadosCamareros: Usuario[] = this.variables.empleadosEmpresa.filter(e => e.departamento.nombreDepartamento == "Camareros");
                console.log("Empleados: ", empleadosCamareros);

                empleadosCamareros.forEach(usuario => {
                  this.firebaseMessaging.enviarNotificacion(usuario, "Cobro en " + this.mesaActual.nombreMesa, "El cliente está esperando el cobro del servicio")
                    .then(() => {
                      console.log("Enviado");
                    })
                    .catch((err) => {
                      console.log(err);

                    })
                });
              })
              .catch(() => {
                this.controlInformacion.mostrarToast("Error al actualizar ticket", "danger");
              })
          }
        },
        {
          text: "TARJETA",
          handler: async () => {
            let totalPago = 0;

            this.ticketMesa.platos.forEach(plato => {
              totalPago = totalPago + plato.precio;
            })

            let modal = await this.modalCtrl.create({
              component: PagarConTarjetaPage,
              componentProps: { cuentaTotal: totalPago }
            })

            modal.present();
            modal.onDidDismiss()
              .then((res) => {
                if (res.data == true) {

                  let empleadosCamareros: Usuario[] = this.variables.empleadosEmpresa.filter(e => e.departamento.nombreDepartamento == "Camareros");
                  console.log("Empleados: ", empleadosCamareros);

                  empleadosCamareros.forEach(usuario => {
                    this.firebaseMessaging.enviarNotificacion(usuario, "Fin servicio, pago con tarjeta, " + this.mesaActual.nombreMesa, "El cliente ha efectuado el pago del servicio, habilitar mesa")
                      .then(() => {
                        console.log("Enviado");
                      })
                      .catch((err) => {
                        console.log(err);

                      })
                  });

                  this.finalizarTicket();
                }
              })

          }
        }
      ]
    })
    alert.present();
  }
  async finalizarTicket() {
    await this.controlInformacion.presentLoading("Finalizando cobro...");
    this.ticketMesa.estado = 2;

    this.mesaActual.ocupada = false;
    let indexMesa = this.estanciaActual.mesas.findIndex(mesafiltrada => mesafiltrada.id == this.mesaActual.id);
    this.estanciaActual.mesas[indexMesa] = this.mesaActual;

    this.firestoreService.addMesaEstancia(this.empresaActual, this.estanciaActual)
      .then(() => {

        this.firestoreService.addTicket(this.empresaActual, this.ticketMesa)
          .then(() => {
            this.controlInformacion.mostrarToast("Cobro realizado", "success");

            this.controlInformacion.dismissLoading();
            this.modalCtrl.dismiss(true);
          })
          .catch(() => {
            this.controlInformacion.dismissLoading();
            this.controlInformacion.mostrarToast("Error al actualizar ticket", "danger");
          })
      })
      .catch((err) => {
        this.controlInformacion.dismissLoading();
        this.controlInformacion.mostrarToast("Error al actualizar mesa", "danger");
        return false;
      })
  }

  comandaPlatosSinRepetir() {

    let comandaAgrupados = [];
    if (this.comandaActual) {
      this.comandaActual.platos.forEach(plato => {
        if (comandaAgrupados.findIndex(platoFiltrado => (platoFiltrado.id == plato.id && !plato.observaciones)) == -1) comandaAgrupados.push(plato);
      });
    }

    return comandaAgrupados;
  }

  comandaTicketsSinRepetir() {
    let ticketAgrupados = [];

    let ticketMesa: Ticket = this.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesaActual.id && ticketFiltrado.estado != 2)[0];

    if (ticketMesa) {
      ticketMesa.platos.forEach((plato) => {
        if (ticketAgrupados.findIndex(platoFiltrado => platoFiltrado.id == plato.id && !plato.observaciones) == -1) ticketAgrupados.push(plato);
      })

    }

    return ticketAgrupados;
  }

  sumaTicketActual() {
    let sumaTotal = 0;
    let ticketMesa: Ticket = this.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesaActual.id && ticketFiltrado.estado != 2)[0];
    sumaTotal = ticketMesa.platos.map(plato => plato.precio).reduce((prev, curr) => prev + curr, 0);
    return sumaTotal.toFixed(2);
  }

  cantidadEnTicket(plato: any): number {
    let ticketMesa: Ticket = this.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesaActual.id && ticketFiltrado.estado != 2)[0];

    let cantidad: number = ticketMesa.platos.filter(platoFiltrado => platoFiltrado.id == plato.id && platoFiltrado.observaciones == plato.observaciones).length;
    return cantidad;
  }

  async comenzarServicio() {
    this.mesaActual.ocupada = true;

    let indexMesa = this.estanciaActual.mesas.findIndex(mesafiltrada => mesafiltrada.id == this.mesaActual.id);
    this.estanciaActual.mesas[indexMesa] = this.mesaActual;

    this.firestoreService.addMesaEstancia(this.empresaActual, this.estanciaActual)
      .then(() => {
        let idTicket = this.tickets.length + 1;
        let ticket: Ticket = { id: idTicket.toString(), platos: [], estado: 0, mesa: this.mesaActual, fechaTicket: moment().format("DD/MM/YY HH:mm") };

        this.firestoreService.addTicket(this.empresaActual, ticket)
          .then(() => {
            console.log("Ticket creado");

            let empleadosCamareros: Usuario[] = this.variables.empleadosEmpresa.filter(e => e.departamento.nombreDepartamento == "Camareros");
            console.log("Empleados: ", empleadosCamareros);

            empleadosCamareros.forEach(usuario => {
              this.firebaseMessaging.enviarNotificacion(usuario, "Nuevo servicio en " + this.mesaActual.nombreMesa, "Atender al cliente")
                .then(() => {
                  console.log("Enviado");
                })
                .catch((err) => {
                  console.log(err);

                })
            });

          })
          .catch(() => {
            this.controlInformacion.mostrarToast("Error al actualizar ticket", "danger");
          })
        this.controlInformacion.mostrarToast("Servicio comenzado", "success");
      })
      .catch((err) => {
        this.controlInformacion.mostrarToast("Error al actualizar mesa", "danger");
        return false;
      })
  }

  pedirPlatos() {
    this.comandaActual = { platos: [], id: "0", fechaComanda: "", mesa: null, estado: 0, nombreEstancia: "" };
    this.pidiendoPlatos = true;
  }

  async addComanda() {
    let idComanda = this.comandas.length + 1;
    this.comandaActual.id = idComanda.toString();
    this.comandaActual.fechaComanda = moment().format("DD/MM/YY HH:mm");
    this.comandaActual.mesa = this.mesaActual;
    this.comandaActual.nombreEstancia = this.estanciaActual.nombreEstancia;
    this.comandaActual.platos.sort((a, b) => (a.nombrePlato > b.nombrePlato) ? 1 : -1);
    this.firestoreService.addComanda(this.empresaActual, this.comandaActual)
      .then((res) => {
        this.controlInformacion.mostrarToast("Comanda creada", "success");

        let indexTicketMesa = this.tickets.findIndex(ticketFiltrado => ticketFiltrado.mesa.id == this.mesaActual.id && ticketFiltrado.estado != 2);
        this.comandaActual.platos.forEach(plato => {
          this.tickets[indexTicketMesa].platos.push(plato);
        })

        this.firestoreService.addTicket(this.empresaActual, this.tickets[indexTicketMesa])
          .then(() => {
            this.controlInformacion.mostrarToast("Comanda enlazada a ticket", "success");
            let empleadosCocina: Usuario[] = this.variables.empleadosEmpresa.filter(e => e.departamento.nombreDepartamento == "Cocina");
            console.log("Empleados: ", empleadosCocina);

            empleadosCocina.forEach(usuario => {
              this.firebaseMessaging.enviarNotificacion(usuario, this.comandaActual.mesa.nombreMesa, "Nueva comanda con " + this.comandaActual.platos.length + " platos")
                .then(() => {
                  console.log("Enviado");
                })
                .catch((err) => {
                  console.log(err);

                })
            });
          })
          .catch((err) => {
            this.controlInformacion.mostrarToast("Error al enlazar comanda", "danger");
          })

        this.pidiendoPlatos = false;
      })
      .catch((err) => {
        this.controlInformacion.mostrarToast("Error al crear comanda", "danger");
        console.error(err);

      })
  }

}
