import { DetalleMesaPage } from './detalleMesa/detalle-mesa/detalle-mesa.page';
import { Estancias } from './../../Modelos/Estancias';
import { Mesa } from './../../Modelos/Mesa';
import { VariablesService } from './../../Servicios/variables.service';
import { AlertController, ModalController } from '@ionic/angular';
import { FirebaseFirestoreService } from './../../Servicios/Firebase/firebase-firestore.service';
import { Component, OnInit } from '@angular/core';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.page.html',
  styleUrls: ['./mesas.page.scss'],
})
export class MesasPage implements OnInit {

  public mesas: Estancias[];

  constructor(
    public firestoreService: FirebaseFirestoreService,
    public alertCtrl: AlertController,
    public variables: VariablesService,
    public controlInformacion: ControlInformacionService,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.cargarCarta();
  }

  async cargarCarta() {
    (await this.firestoreService.getEstanciasRestaurante(this.variables.empresaActual)).subscribe((estancias) => {
      console.log("Estancias: ", estancias);
      this.mesas = estancias as any;
    })
  }

  async addNuevaEstancia() {
    let alert = await this.alertCtrl.create({
      header: "Nueva estancia",
      message: "Añadir nueva estancia",
      inputs: [{
        placeholder: "Nombre de la estancia",
        label: "Estancia",
        name: "estancia"
      }],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Crear",
          handler: async (datos) => {
            let nombreEstancia: string = titleCaseWord(datos.estancia);

            function titleCaseWord(word: string) {
              if (!word) return word;
              return word[0].toUpperCase() + word.substr(1).toLowerCase();
            }

            if (this.mesas.filter(estancia => estancia.nombreEstancia.toLowerCase() == nombreEstancia.toLowerCase()).length == 0) {
              let estanciaNueva: Estancias = { nombreEstancia: nombreEstancia, mesas: [] };
              await this.firestoreService.addEstanciaNueva(this.variables.empresaActual, estanciaNueva)
                .then(() => {
                  this.controlInformacion.mostrarToast("Estancia añadida", "success");
                })
                .catch((err) => {
                  this.controlInformacion.mostrarToast("Error al añadir estancia", "danger");
                })
            } else {
              this.controlInformacion.mostrarToast("La estancia ya está creada", "danger");
              return false
            }
          }
        }
      ]
    })

    alert.present();
  }

  ticketPendienteDePago(mesa: Mesa){
    if(this.variables.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == mesa.id && ticketFiltrado.estado == 1).length > 0) return true
    else return false;
  }


  async addMesaEstancia(estancia: Estancias) {
    let alert = await this.alertCtrl.create({
      header: "Nueva mesa",
      message: "Añadir nueva mesa",
      inputs: [{
        placeholder: "Nombre de la mesa",
        label: "Mesa",
        name: "mesa"
      }],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Añadir",
          handler: async (datos) => {
            let nombreMesa: string = titleCaseWord(datos.mesa);

            function titleCaseWord(word: string) {
              if (!word) return word;
              return word[0].toUpperCase() + word.substr(1).toLowerCase();
            }

            if (estancia.mesas.filter(estancia => estancia.nombreMesa.toLowerCase() == nombreMesa.toLowerCase()).length == 0) {
              let mesa: Mesa = { nombreMesa: nombreMesa, ocupada: false, id: nombreMesa.replace(/\s/g, '') };
              estancia.mesas.push(mesa);

              await this.firestoreService.addMesaEstancia(this.variables.empresaActual, estancia)
                .then(() => {
                  this.controlInformacion.mostrarToast("Mesa añadida", "success");
                  return true;
                })
                .catch((err) => {
                  this.controlInformacion.mostrarToast("Error al añadir mesa", "danger");
                })
            } else {
              this.controlInformacion.mostrarToast("La mesa ya está creada", "danger");
              return false;
            }
          }
        }
      ]
    })

    alert.present();
  }

  async pulsarMesa(mesa: Mesa, mesas: Estancias) {

    let modal = await this.modalCtrl.create({
      component: DetalleMesaPage,
      componentProps: {mesa: mesa, estancia:mesas}
    })

    modal.present();

  }

}
