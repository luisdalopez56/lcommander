import { VariablesService } from './../../../../../Servicios/variables.service';
import { Mesa } from 'src/app/Modelos/Mesa';
import { NavParams, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ver-qr',
  templateUrl: './ver-qr.page.html',
  styleUrls: ['./ver-qr.page.scss'],
})
export class VerQRPage implements OnInit {

  idMesa: string
  mesa: Mesa;
  urlQR: string;
  constructor(
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public variables: VariablesService
  ) {
    this.mesa = navParams.data.mesa;
    this.idMesa = this.mesa.id;

    this.urlQR = "https://lcommander-203bf.web.app/clientes?guidEmpresa=" + this.variables.empresaActual.
    NIFEmpresa + "&idMesa=" + this.idMesa;
  }

  ngOnInit() {

  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }

  imprimirQR() {
    let popupWinindow;
    let innerContents = document.getElementById("qrCode").innerHTML;
    popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()"><div style="text-align:center">' + innerContents + '</div></html>');
    popupWinindow.document.close();
  }


}
