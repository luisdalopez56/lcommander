import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleMesaPage } from './detalle-mesa.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleMesaPage
  },
  {
    path: 'ver-qr',
    loadChildren: () => import('./ver-qr/ver-qr.module').then( m => m.VerQRPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleMesaPageRoutingModule {}
