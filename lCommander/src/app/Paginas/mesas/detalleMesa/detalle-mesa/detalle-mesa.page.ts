import { RealizarCobroPage } from './../realizar-cobro/realizar-cobro.page';
import { VerQRPage } from './ver-qr/ver-qr.page';
import { PlatoComanda } from './../../../../Modelos/Comanda';
import { Ticket } from './../../../../Modelos/Ticket';
import { PedirPlatosPage } from './../pedirPlatos/pedir-platos/pedir-platos.page';
import { AddPlatoPage } from './../../../carta/addPlato/add-plato/add-plato.page';
import { CategoriaPlatos } from './../../../../Modelos/CategoriaPlatos';
import { Estancias } from './../../../../Modelos/Estancias';
import { VariablesService } from './../../../../Servicios/variables.service';
import { FirebaseFirestoreService } from './../../../../Servicios/Firebase/firebase-firestore.service';
import { Mesa } from './../../../../Modelos/Mesa';
import { NavParams, AlertController, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';
import { CartaPage } from 'src/app/Paginas/carta/carta.page';
import { Comanda } from 'src/app/Modelos/Comanda';
import { Plato } from 'src/app/Modelos/Plato';
import * as moment from 'moment';

@Component({
  selector: 'app-detalle-mesa',
  templateUrl: './detalle-mesa.page.html',
  styleUrls: ['./detalle-mesa.page.scss'],
})
export class DetalleMesaPage implements OnInit {

  public mesa: Mesa;
  public estancia: Estancias;
  public ticketMesa: Ticket = new Ticket();
  public carta: CategoriaPlatos[];
  public mostrarDetalleCuenta: boolean = false;
  public comandasPendientes: Comanda[];
  constructor(
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public firestoreService: FirebaseFirestoreService,
    public variables: VariablesService,
    public controlInformacion: ControlInformacionService,
    public modalCtrl: ModalController
  ) {
    this.mesa = this.navParams.data.mesa;
    this.estancia = this.navParams.data.estancia;
  }

  ngOnInit() {
    this.cargarCarta()
  }

  /**
   * Abre la página que genera el QR de la mesa
   */
  async abrirQR() {
    let modal = await this.modalCtrl.create({
      component: VerQRPage,
      componentProps: { mesa: this.mesa }
    })

    modal.present()
  }
  /**
   * Carga la carta del restaurante
   */
  async cargarCarta() {
    (await this.firestoreService.getCartaRestaurante(this.variables.empresaActual))
      .subscribe((carta) => {
        this.variables.carta = carta as CategoriaPlatos[];
      })
  }

  /**
   * Devuelve las comandas pednientes de la mesa
   * @returns {Comanda[]}
   */
  buscarComandasPendientes() {
    this.comandasPendientes = this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.mesa.id == this.mesa.id && comandaFiltrada.estado == 0);
    this.comandasPendientes = this.comandasPendientes.sort((a, b) => (a.fechaComanda > b.fechaComanda) ? 1 : -1)

    return this.comandasPendientes.length;
  }

  /**
   * Devuelve el numero de platos que hay en la comanda
   * @param plato Plato a buscar
   * @param comanda Comanda en la que buscas
   * @returns {number}
   */
  cantidadDePlatos(plato: PlatoComanda, comanda: Comanda) {

    let cantidad: number;
    if (plato.observaciones) {
      cantidad = comanda.platos.filter(platoFiltrado => platoFiltrado.id == plato.id && platoFiltrado.observaciones == plato.observaciones).length;
    } else {
      cantidad = comanda.platos.filter(platoFiltrado => platoFiltrado.id == plato.id).length;
    }

    return cantidad;

  }

  /**
   * Abre la página para añadir platos a una nueva comandas
   */
  async addProductosComanda() {
    let modal = await this.modalCtrl.create({
      component: PedirPlatosPage,
      componentProps: { mesa: this.mesa, estancia: this.estancia }
    })

    modal.present();
  }

  /**
   * Cierra la pagina en modal
   */
  cerrarModal() {
    this.modalCtrl.dismiss();
  }

  /**
   * Devuelve los platos agrupados sin repetir
   * @param comanda Comanda en la que se va a buscar
   * @returns {Comanda}
   */
  platosSinRepetir(comanda: Comanda): PlatoComanda[] {
    let comandaAgrupados = [];
    comanda.platos.forEach(plato => {
      console.log(plato);
      if (comandaAgrupados.findIndex(platoFiltrado => (platoFiltrado.id == plato.id && !plato.observaciones && plato.estado != 3)) == -1) comandaAgrupados.push(plato);
    });


    return comandaAgrupados;
  }

  /**
   * Se inicia el servicio en la mesa
   */
  async comenzarServicio() {
    let alert = await this.alertCtrl.create({
      header: "Comenzar servicio",
      message: "¿Seguro que quieres comenzar el servicio en esta mesa?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {

            let idTicket = this.variables.tickets.length + 1;
            let ticket: Ticket = { id: idTicket.toString(), platos: [], estado: 0, mesa: this.mesa, fechaTicket: moment().format("DD/MM/YY HH:mm") };
            this.ticketMesa = ticket;

            this.mesa.ocupada = true;
            let indexMesa = this.estancia.mesas.findIndex(mesafiltrada => mesafiltrada.id == this.mesa.id);
            this.estancia.mesas[indexMesa] = this.mesa;

            this.firestoreService.addMesaEstancia(this.variables.empresaActual, this.estancia)
              .then(() => {


                this.firestoreService.addTicket(this.variables.empresaActual, ticket)
                  .then(() => {
                    console.log("Ticket creado");
                  })
                  .catch(() => {
                    this.controlInformacion.mostrarToast("Error al actualizar ticket", "danger");
                  })
                this.controlInformacion.mostrarToast("Servicio comenzado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al actualizar mesa", "danger");
                return false;
              })
          }
        }
      ]
    })

    alert.present();
  }

  /**
   * Devuelve la suma del ticket
   * @returns {number}
   */
  sumaTicketActual() {
    let sumaTotal = 0;
    let ticketMesa: Ticket = this.variables.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesa.id && ticketFiltrado.estado != 2)[0];
    console.log(ticketMesa);

    if (ticketMesa) sumaTotal = ticketMesa.platos.map(plato => plato.precio).reduce((prev, curr) => prev + curr, 0);

    return sumaTotal.toFixed(2);
  }

  /**
   * Abre la página para realizar el cobro en la mesa
   */
  async realizarCobro() {
    let modal = await this.modalCtrl.create({
      component: RealizarCobroPage,
      componentProps: { tipoPago: "Efectivo", ticket: this.ticketMesa, mesa: this.mesa, estancia: this.estancia }
    })

    modal.present();

    modal.onDidDismiss()
      .then((res) => {
        console.log(res);

      })
  }
  /**
   * Devuelve los platos del ticket sin repetir
   * @returns {Plato[]}
   */
  comandaPlatosSinRepetir() {
    this.ticketMesa = this.variables.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesa.id && ticketFiltrado.estado != 2)[0];

    let ticketAgrupados = [];

    let ticketMesa: Ticket = this.variables.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesa.id && (ticketFiltrado.estado == 0 || ticketFiltrado.estado == 1))[0];
    console.log("ticket en mesa:", ticketMesa);
    console.log("Mesa actual: ", this.mesa);

    if (ticketMesa) {
      ticketMesa.platos.forEach((plato) => {
        if (ticketAgrupados.findIndex(platoFiltrado => platoFiltrado.id == plato.id && !plato.observaciones) == -1) ticketAgrupados.push(plato);
      })

    }

    return ticketAgrupados;
  }

  /**
   * Cantidad de platos en el ticket
   * @param plato Plato en el que buscar
   * @returns {number}
   */
  cantidadEnTicket(plato: any): number {
    let ticketMesa: Ticket = this.variables.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesa.id && ticketFiltrado.estado != 2)[0];

    let cantidad: number = ticketMesa.platos.filter(platoFiltrado => platoFiltrado.id == plato.id && platoFiltrado.observaciones == plato.observaciones).length;
    return cantidad;
  }

  /**
   * Finaliza el servicio de la mesa
   */
  finalizarServicio() {
    let ticketMesa: Ticket = this.variables.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == this.mesa.id)[0];
    ticketMesa.estado = 1;
    ticketMesa.precioTotal = Number.parseFloat(this.sumaTicketActual());
    this.firestoreService.addTicket(this.variables.empresaActual, ticketMesa)
      .then(() => {
        console.log("Ticket actualizado");
      })
      .catch(() => {
        this.controlInformacion.mostrarToast("Error al actualizar ticket", "danger");
      })
  }

}
