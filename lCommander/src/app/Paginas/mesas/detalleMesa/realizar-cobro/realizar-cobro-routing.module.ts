import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RealizarCobroPage } from './realizar-cobro.page';

const routes: Routes = [
  {
    path: '',
    component: RealizarCobroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RealizarCobroPageRoutingModule {}
