import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RealizarCobroPageRoutingModule } from './realizar-cobro-routing.module';

import { RealizarCobroPage } from './realizar-cobro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RealizarCobroPageRoutingModule
  ],
  declarations: [RealizarCobroPage]
})
export class RealizarCobroPageModule {}
