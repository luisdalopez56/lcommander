import { Estancias } from 'src/app/Modelos/Estancias';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';
import { VariablesService } from 'src/app/Servicios/variables.service';
import { FirebaseFirestoreService } from 'src/app/Servicios/Firebase/firebase-firestore.service';
import { Ticket } from 'src/app/Modelos/Ticket';
import { NavParams, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Mesa } from 'src/app/Modelos/Mesa';

@Component({
  selector: 'app-realizar-cobro',
  templateUrl: './realizar-cobro.page.html',
  styleUrls: ['./realizar-cobro.page.scss'],
})
export class RealizarCobroPage implements OnInit {

  tipoPago: "Efectivo" | "Tarjeta";
  ticket: Ticket;
  mesa: Mesa;
  cantidadEntregada: number = 0;
  Math = Math;
  estancia: Estancias;
  constructor(
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public variables: VariablesService,
    public controlInformacion: ControlInformacionService,
    public firestoreService: FirebaseFirestoreService
  ) {
    this.tipoPago = this.navParams.data.tipoPago;
    this.ticket = this.navParams.data.ticket;
    this.mesa = this.navParams.data.mesa;
    this.estancia =this.navParams.data.estancia;
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }

  finalizarCobro() {
    this.ticket.estado = 2;

    this.mesa.ocupada = false;
    let indexMesa = this.estancia.mesas.findIndex(mesafiltrada => mesafiltrada.id == this.mesa.id);
    this.estancia.mesas[indexMesa] = this.mesa;

    this.firestoreService.addMesaEstancia(this.variables.empresaActual, this.estancia)
      .then(() => {

        this.firestoreService.addTicket(this.variables.empresaActual, this.ticket)
          .then(() => {
            this.modalCtrl.dismiss(true);
          })
          .catch(() => {
            this.controlInformacion.mostrarToast("Error al actualizar ticket", "danger");
          })
        this.controlInformacion.mostrarToast("Cobro realizado", "success");
      })
      .catch((err) => {
        this.controlInformacion.mostrarToast("Error al actualizar mesa", "danger");
        return false;
      })
  }

  ngOnInit() {
  }

}
