import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedirPlatosPageRoutingModule } from './pedir-platos-routing.module';

import { PedirPlatosPage } from './pedir-platos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedirPlatosPageRoutingModule
  ],
  declarations: [PedirPlatosPage]
})
export class PedirPlatosPageModule {}
