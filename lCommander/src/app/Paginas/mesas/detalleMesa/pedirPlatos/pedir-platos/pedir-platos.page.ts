import { Comanda, PlatoComanda } from './../../../../../Modelos/Comanda';
import { VariablesService } from './../../../../../Servicios/variables.service';
import { Component, OnInit } from '@angular/core';
import { Plato } from 'src/app/Modelos/Plato';
import { AlertController, ModalController, NavParams } from '@ionic/angular';
import { FirebaseFirestoreService } from 'src/app/Servicios/Firebase/firebase-firestore.service';
import * as moment from 'moment';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';
import { Mesa } from 'src/app/Modelos/Mesa';
import { Estancias } from 'src/app/Modelos/Estancias';

@Component({
  selector: 'app-pedir-platos',
  templateUrl: './pedir-platos.page.html',
  styleUrls: ['./pedir-platos.page.scss'],
})
export class PedirPlatosPage implements OnInit {

  comandaActual: Comanda = new Comanda();
  verTicketComanda: boolean = false;
  mesaActual: Mesa;
  estanciaActual: Estancias;
  platoClickado: string;
  constructor(
    public variables: VariablesService,
    public modalCtrl: ModalController,
    public firestoreService: FirebaseFirestoreService,
    public navParams: NavParams,
    public controlInformacionService: ControlInformacionService,
    public alertCtrl: AlertController,

  ) {
    this.comandaActual = { platos: [], id: "0", fechaComanda: "", mesa: null, estado: 0, nombreEstancia: "" };
    this.mesaActual = this.navParams.data.mesa;
    this.estanciaActual = this.navParams.data.estancia;

  }

  ngOnInit() {
  }

  filterPlatosDisponibles(platosLista: Plato[]): Plato[] {
    let result: Plato[] = [];

    result = platosLista.filter(platoFiltrado => platoFiltrado.disponible);

    return result;
  }

  clickarPlato(plato: Plato) {
    console.log("clickado");

    if (this.platoClickado != plato.id) {
      this.platoClickado = plato.id;
    } else {
      this.platoClickado = null;
    }
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }

  async addComanda() {
    let idComanda = this.variables.comandas.length + 1;
    this.comandaActual.id = idComanda.toString();
    this.comandaActual.fechaComanda = moment().format("DD/MM/YY HH:mm");
    this.comandaActual.mesa = this.mesaActual;
    this.comandaActual.nombreEstancia = this.estanciaActual.nombreEstancia;
    this.comandaActual.platos.sort((a, b) => (a.nombrePlato > b.nombrePlato) ? 1 : -1);
    this.firestoreService.addComanda(this.variables.empresaActual, this.comandaActual)
      .then((res) => {
        this.controlInformacionService.mostrarToast("Comanda creada", "success");

        let indexTicketMesa = this.variables.tickets.findIndex(ticketFiltrado => ticketFiltrado.mesa.id == this.mesaActual.id && ticketFiltrado.estado != 2);
        this.comandaActual.platos.forEach(plato => {
          this.variables.tickets[indexTicketMesa].platos.push(plato);
        })

        this.firestoreService.addTicket(this.variables.empresaActual, this.variables.tickets[indexTicketMesa])
          .then(() => {
            this.controlInformacionService.mostrarToast("Comanda enlazada a ticket", "success");
          })
          .catch((err) => {
            this.controlInformacionService.mostrarToast("Error al enlazar comanda", "danger");
          })

        this.modalCtrl.dismiss();
      })
      .catch((err) => {
        this.controlInformacionService.mostrarToast("Error al crear comanda", "danger");
        console.error(err);

      })
  }

  addPlatoComanda(plato: Plato, observaciones?: string) {
    let platoComanda: PlatoComanda = { id: plato.id, precio: plato.precio, estado: 0, nombrePlato: plato.nombrePlato };
    this.comandaActual.platos.push(platoComanda);
  }

  deletePlatoComanda(plato: Plato) {
    let findIndex = this.comandaActual.platos.findIndex(platoFiltrado => platoFiltrado.id == plato.id);
    this.comandaActual.platos.splice(findIndex, 1);
  }

  cantidadEnComanda(plato: any): number {
    let cantidad: number;
    if(plato.observaciones){
      cantidad = this.comandaActual.platos.filter(platoFiltrado => platoFiltrado.id == plato.id && platoFiltrado.observaciones == plato.observaciones).length;
    }else{
      cantidad = this.comandaActual.platos.filter(platoFiltrado => platoFiltrado.id == plato.id).length;
    }

    return cantidad;
  }

  async addPlatoEspecial(plato: Plato) {
    let alert = await this.alertCtrl.create({
      header: "Plato espcial",
      message: "Añadir instrucciones",
      inputs: [{
        type:"textarea",
        placeholder: "Añade instrucciones",
        label: "observaciones",
        name: "observaciones"
      }],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Añadir a comanda",
          handler: async (datos) => {
            let platoComanda: PlatoComanda = { id: plato.id, precio: plato.precio, estado: 0, nombrePlato: plato.nombrePlato,observaciones: datos.observaciones };
            this.comandaActual.platos.push(platoComanda);
          }

        }
      ]
    })

    alert.present();
  }

  comandaPlatosSinRepetir() {

    let comandaAgrupados = [];
    this.comandaActual.platos.forEach(plato => {
      console.log(plato);
      if (comandaAgrupados.findIndex(platoFiltrado => (platoFiltrado.id == plato.id && !plato.observaciones)) == -1) comandaAgrupados.push(plato);
    });

    return comandaAgrupados;
  }

  cuentaTotal(): number {
    let cuenta = 0;
    this.comandaActual.platos.forEach(plato => {
      cuenta = cuenta + plato.precio;
    })
    return cuenta;
  }
}
