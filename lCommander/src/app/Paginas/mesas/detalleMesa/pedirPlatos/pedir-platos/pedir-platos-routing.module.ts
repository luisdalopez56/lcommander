import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedirPlatosPage } from './pedir-platos.page';

const routes: Routes = [
  {
    path: '',
    component: PedirPlatosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedirPlatosPageRoutingModule {}
