import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MesasPage } from './mesas.page';

const routes: Routes = [
  {
    path: '',
    component: MesasPage
  },
  {
    path: 'detalle-mesa',
    loadChildren: () => import('./detalleMesa/detalle-mesa/detalle-mesa.module').then( m => m.DetalleMesaPageModule)
  },
  {
    path: 'pedir-platos',
    loadChildren: () => import('./detalleMesa/pedirPlatos/pedir-platos/pedir-platos.module').then( m => m.PedirPlatosPageModule)
  },
  {
    path: 'realizar-cobro',
    loadChildren: () => import('./detalleMesa/realizar-cobro/realizar-cobro.module').then( m => m.RealizarCobroPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MesasPageRoutingModule {}
