import { FirebaseFCMService } from 'src/app/Servicios/Firebase/firebase-fcm.service';
import { FirebaseFirestoreService } from 'src/app/Servicios/Firebase/firebase-firestore.service';
import { AlertController } from '@ionic/angular';
import { PlatoComanda } from './../../Modelos/Comanda';
import { VariablesService } from './../../Servicios/variables.service';
import { Component, OnInit } from '@angular/core';
import { Comanda } from 'src/app/Modelos/Comanda';
import * as moment from 'moment';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';
import { Usuario } from 'src/app/Modelos/Usuario';
@Component({
  selector: 'app-cocina',
  templateUrl: './cocina.page.html',
  styleUrls: ['./cocina.page.scss'],
})
export class CocinaPage implements OnInit {

  moment: any = moment;
  fechaHoy = moment().format("DD/MM/YYYY mm:ss");
  comandaPulsada: string;
  platoPulsado: number;
  date = Date;

  constructor(
    public variables: VariablesService,
    public alertCtrl: AlertController,
    public firestoreService: FirebaseFirestoreService,
    public controlInformacion: ControlInformacionService,
    public firebaseMessaging: FirebaseFCMService
  ) { }

  ngOnInit() {
  }

  diff_minutes(dt1) {
    var time = new Date().getTime() - new Date(dt1).getTime();
    // console.log(time);

    function msToTime(duration) {
      // var milliseconds = Math.floor((duration % 1000) / 100);
      let seconds = Math.floor((duration / 1000) % 60);
       let minutes = Math.floor((duration / (1000 * 60)) % 60);
       let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

     let hoursF = (hours < 10) ? "0" + hours : hours;
     let minutesF = (minutes < 10) ? "0" + minutes : minutes;
     let secondsF = (seconds < 10) ? "0" + seconds : seconds;

     let textFinal ="";
      if(hoursF > 0) textFinal = textFinal + hoursF + "hr ";
      textFinal = textFinal + minutesF + "min";

      return textFinal;
    }

    return msToTime(time);
  }

  comandasPendientes(): Comanda[] {
    this.variables.comandas = this.variables.comandas.sort((a, b) => (a.fechaComanda > b.fechaComanda) ? 1 : -1);
    return this.variables.comandas.filter(comandaFiltro => comandaFiltro.estado == 0);
  }

  platoPulsadoComanda(comanda: Comanda, numeroPlato: number) {
    if (this.comandaPulsada == comanda.id && numeroPlato == this.platoPulsado) {
      this.comandaPulsada = null;
      this.platoPulsado = null;
    }
    else {
      this.comandaPulsada = comanda.id;
      this.platoPulsado = numeroPlato;
    }
  }

  async marcarPlatoPreparando(plato: PlatoComanda, comanda: Comanda, index: number) {
    let alert = await this.alertCtrl.create({
      header: "¿Preparando plato?",
      message: "Estas apunto de poner este plato en preparación, ¿estás seguro?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].estado = 1;
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].usuarioCocina = this.variables.empleadoSesion;
            this.firestoreService.addComanda(this.variables.empresaActual, this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0])
              .then(() => {
                this.controlInformacion.mostrarToast("Estado del plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
              })
          }
        }
      ]
    })

    alert.present();
  }

  async marcarPlatoLlevarAMesa(plato: PlatoComanda, comanda: Comanda, index: number) {
    let alert = await this.alertCtrl.create({
      header: "¿Plato listo?",
      message: "Estas apunto de poner este plato para entregar al cliente, ¿estás seguro?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].estado = 2;
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].usuarioCocina = this.variables.empleadoSesion;
            this.firestoreService.addComanda(this.variables.empresaActual, this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0])
              .then(() => {
console.log(this.variables.empleadosEmpresa);

                let empleadosCamareros: Usuario[] = this.variables.empleadosEmpresa.filter(e => e.departamento.id == 2);
                console.log("Empleados: ", empleadosCamareros);

                empleadosCamareros.forEach(usuario => {
                  this.firebaseMessaging.enviarNotificacion(usuario, comanda.mesa.nombreMesa, plato.nombrePlato + " está listo para entregar al cliente en " + comanda.mesa.nombreMesa)
                    .then(() => {
                      console.log("Enviado");
                    })
                    .catch((err) => {
                      console.log(err);

                    })
                });

                this.controlInformacion.mostrarToast("Estado del plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
              })
          }
        }
      ]
    })

    alert.present();
  }

  async marcarPlatoEnEspera(plato: PlatoComanda, comanda: Comanda, index: number) {
    let alert = await this.alertCtrl.create({
      header: "¿Plato en espera?",
      message: "Estas apunto de poner este plato en espera de nuevo, ¿estás seguro?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].estado = 0;
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].usuarioCocina = null;
            this.firestoreService.addComanda(this.variables.empresaActual, this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0])
              .then(() => {
                this.controlInformacion.mostrarToast("Estado del plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
              })
          }
        }

      ]
    })

    alert.present();
  }

}
