import { VariablesService } from './../../Servicios/variables.service';
import { Component, OnInit } from '@angular/core';
import { Ticket } from 'src/app/Modelos/Ticket';
import * as moment from 'moment';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.page.html',
  styleUrls: ['./tickets.page.scss'],
})
export class TicketsPage implements OnInit {
  monthShortNames = "Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre";
  fechaSeleccionada: string;
  constructor(
    public variables: VariablesService
  ) { }

  ngOnInit() {
  }

  cantidadEnTicket(ticket: Ticket, plato: any): number {
    let cantidad: number = ticket.platos.filter(platoFiltrado => platoFiltrado.id == plato.id && platoFiltrado.observaciones == plato.observaciones).length;
    return cantidad;
  }

  filtrarTickets() {
    let tickets = this.variables.tickets;

    if (this.fechaSeleccionada) {

      tickets = tickets.filter(ticketfiltrado => ticketfiltrado.fechaTicket.substr(0,8) == moment(this.fechaSeleccionada).format("DD/MM/YY")
      )
    }
    return tickets;
  }

  sumaTicketActual(ticket:Ticket) {
    let sumaTotal = 0;
    let ticketMesa: Ticket = this.variables.tickets.filter(ticketFiltrado => ticketFiltrado.mesa.id == ticket.mesa.id && ticketFiltrado.id == ticket.id)[0];
    sumaTotal = ticketMesa.platos.map(plato => plato.precio).reduce((prev, curr) => prev + curr, 0);
    return sumaTotal.toFixed(2);
  }

  comandaPlatosSinRepetir(ticket: Ticket) {
    let ticketAgrupados = [];

    ticket.platos.forEach((plato) => {
      if (ticketAgrupados.findIndex(platoFiltrado => platoFiltrado.id == plato.id && !plato.observaciones) == -1) ticketAgrupados.push(plato);
    })
    return ticketAgrupados;
  }

}
