import { FirebaseAuthService } from './../../../Servicios/Firebase/firebase-auth.service';
import { FirebaseFirestoreService } from './../../../Servicios/Firebase/firebase-firestore.service';
import { Usuario } from './../../../Modelos/Usuario';
import { ModalController } from '@ionic/angular';
import { VariablesService } from './../../../Servicios/variables.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';

@Component({
  selector: 'app-add-empleado',
  templateUrl: './add-empleado.page.html',
  styleUrls: ['./add-empleado.page.scss'],
})
export class AddEmpleadoPage implements OnInit {

  public formularioNuevoEmpleado: FormGroup
  public validaciones_mensajes_formulario = {
    'nombreApellidos': [
      { type: 'required', message: 'El nombre y apellidos no puede estar vacío' },
    ],
    'departamento': [
      { type: 'required', message: 'La descripción no puede estar vacía' }
    ],
    'email': [
      { type: 'required', message: 'El email no puede estar vacío' },
      { type: 'email', message: 'Formato incorrecto' }
    ],
    'password': [
      { type: 'required', message: 'La contraseña no puede estar vacía' }
    ],
  }
  constructor(
    public formBuilder: FormBuilder,
    public variables: VariablesService,
    public modalCtrl: ModalController,
    public controlInformacion: ControlInformacionService,
    public firestoreService: FirebaseFirestoreService,
    public firebaseAuth: FirebaseAuthService
  ) {
    this.formularioNuevoEmpleado = this.crearFormulario();
  }

  ngOnInit() {
  }

  crearFormulario() {
    return this.formBuilder.group({
      nombreApellidos: ["", Validators.required],
      departamento: ["", Validators.required],
      email: ["", [Validators.email, Validators.required]],
      password: ["", [Validators.required, Validators.min(0.1)]],
    })
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }

  async crearEmpleado() {
    await this.controlInformacion.presentLoading("Creando usuario...")

    let nuevoEmpleado: Usuario = {
      email: this.formularioNuevoEmpleado.value.email,
      nombre: this.formularioNuevoEmpleado.value.nombreApellidos,
      password: this.formularioNuevoEmpleado.value.password,
      departamento: this.variables.departamentos.filter(departamentoFiltrado => departamentoFiltrado.id == this.formularioNuevoEmpleado.value.departamento)[0],
      listaToken: []
    }
    this.firestoreService.addUsuarioEmpresa(this.variables.empresaActual, nuevoEmpleado)
      .then((res) => {
        this.firebaseAuth.addUsuario(nuevoEmpleado)
          .then((res) => {
            this.controlInformacion.mostrarToast("Empleado registrado", "success");
            this.controlInformacion.dismissLoading();
            this.cerrarModal();
          })
          .catch((err) => {
            this.controlInformacion.mostrarToast("Error al crear empleado", "danger");
            this.controlInformacion.dismissLoading();
          })
      })
      .catch((err) => {
        this.controlInformacion.mostrarToast("Error al crear empleado", "danger");
        this.controlInformacion.dismissLoading();
      })
  }

}
