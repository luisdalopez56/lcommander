import { FirebaseAuthService } from './../../Servicios/Firebase/firebase-auth.service';
import { AddEmpleadoPage } from './add-empleado/add-empleado.page';
import { ModalController, AlertController } from '@ionic/angular';
import { Usuario } from './../../Modelos/Usuario';
import { VariablesService } from './../../Servicios/variables.service';
import { FirebaseFirestoreService } from 'src/app/Servicios/Firebase/firebase-firestore.service';
import { Component, OnInit } from '@angular/core';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.page.html',
  styleUrls: ['./empleados.page.scss'],
})
export class EmpleadosPage implements OnInit {

  public empleadoSeleccionado: Usuario = null;
  pwdParaCambiar;
  nuevoDepartamento = "0";
  constructor(
    public firestoreService: FirebaseFirestoreService,
    public variablesService: VariablesService,
    public controlInformacion: ControlInformacionService,
    public firebaseAuth: FirebaseAuthService,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.cargarEmpleado();
  }

  async cargarEmpleado() {
    await (await this.firestoreService.getEmpleadosEmpresa(this.variablesService.empresaActual)).subscribe
      ((empleados) => {
        this.variablesService.empleadosEmpresa = empleados as Usuario[];
        this.empleadoSeleccionado = this.variablesService.empleadoSesion;
        this.nuevoDepartamento = this.empleadoSeleccionado.departamento.id.toString();
      }, (err) => {
        this.controlInformacion.mostrarToast("Error al cargar empleados", "danger");
      })

    this.pwdParaCambiar = this.variablesService.empleadoSesion.password;
  }
  departamentoActual(empleado: Usuario) {
    if (this.variablesService.departamentos.length > 0) {
      return this.variablesService.departamentos.filter(departamentoFiltrado => departamentoFiltrado.id == empleado.departamento.id)[0]
    }
  }

  async eliminarEmpleado(usuario: Usuario) {
    console.log(usuario);

    let alert = await this.alertCtrl.create({
      header: "¿Estás seguro?",
      message: "Borrarás sus datos permanentemente",
      buttons: [
        { text: "Cancelar", role: "cancel" },
        {
          text: "Eliminar", handler: () => {
            this.firestoreService.deleteEmpleado(this.variablesService.empresaActual, usuario)
            .then((res) => {
              this.firebaseAuth.deleteUsuario(usuario);
            })
            .catch((err) => {
              this.controlInformacion.mostrarToast("Error al eliminar empleado","danger")
            })
          }
        }
      ]
    })

    alert.present();
  }

  async actualizarEmpleado() {

    if (this.pwdParaCambiar != this.variablesService.empleadoSesion.password) this.variablesService.empleadoSesion.password = this.pwdParaCambiar;
    if (this.nuevoDepartamento != this.empleadoSeleccionado.departamento.id.toString()) {
      this.empleadoSeleccionado.departamento = this.variablesService.departamentos.filter(departamentoFilter => departamentoFilter.id.toString() == this.nuevoDepartamento)[0]
    }

    await this.controlInformacion.presentLoading("Modificando usuario...");
    this.firestoreService.modificarEmpleado(this.variablesService.empresaActual, this.empleadoSeleccionado)
      .then((res) => {
        if (this.pwdParaCambiar != this.variablesService.empleadoSesion.password) this.firebaseAuth.modificarPwd(this.variablesService.empleadoSesion);


        this.controlInformacion.mostrarToast("Usuario actualizado", "success");
        this.controlInformacion.dismissLoading();
      })
      .catch((err) => {

      })
  }

  comprobarPwd() {
    // console.log(this.variablesService.empleadoSesion, this.nuevoDepartamento);

    return this.variablesService.empleadoSesion.password == this.pwdParaCambiar && this.empleadoSeleccionado.departamento.id.toString() == this.nuevoDepartamento;
  }

  async addNuevoEmpleado() {
    let modal = await this.modalCtrl.create({
      component: AddEmpleadoPage
    })

    modal.present();
  }
}
