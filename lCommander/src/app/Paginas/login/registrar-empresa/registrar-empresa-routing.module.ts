import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrarEmpresaPage } from './registrar-empresa.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrarEmpresaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrarEmpresaPageRoutingModule {}
