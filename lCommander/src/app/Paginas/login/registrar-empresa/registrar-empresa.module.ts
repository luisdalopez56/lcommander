import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrarEmpresaPageRoutingModule } from './registrar-empresa-routing.module';

import { RegistrarEmpresaPage } from './registrar-empresa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrarEmpresaPageRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [RegistrarEmpresaPage]
})
export class RegistrarEmpresaPageModule {}
