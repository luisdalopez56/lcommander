import { Router } from '@angular/router';
import { Usuario } from './../../../Modelos/Usuario';
import { FirebaseAuthService } from './../../../Servicios/Firebase/firebase-auth.service';
import { Empresa } from 'src/app/Modelos/Empresa';
import { ControlInformacionService } from './../../../Servicios/control-informacion.service';
import { FirebaseFirestoreService } from './../../../Servicios/Firebase/firebase-firestore.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registrar-empresa',
  templateUrl: './registrar-empresa.page.html',
  styleUrls: ['./registrar-empresa.page.scss'],
})
export class RegistrarEmpresaPage implements OnInit {
  public formularioNuevaEmpresa: FormGroup;
  public formularioAdministrador: FormGroup

  public validaciones_mensajes_formulario = {
    'nombreEmpresa': [
      { type: 'required', message: 'El nombre de empresa no puede estar vacío' },
    ],
    'nifEmpresa': [
      { type: 'required', message: 'El NIF no puede estar vacío' }
    ],
    'email': [
      { type: 'required', message: 'El email no puede estar vacío' },
      { type: 'email', message: 'El email no tiene el formato correcto' },
    ],
    'nombre': [
      { type: 'required', message: 'El nombre no puede estar vacío' }
    ],
    'apellidos': [
      { type: 'required', message: 'Los apellidos no pueden estar vacios' }
    ],
    'password': [
      { type: 'required', message: 'La contraseña no puede estar vacía' }
    ],

  }


  public primerFormCorrecto: boolean = false;
  constructor(
    public formBuilder: FormBuilder,
    public firebaseFirestoreService: FirebaseFirestoreService,
    public firebaseAuthService: FirebaseAuthService,
    public controlInformacionService: ControlInformacionService,
    public router: Router
  ) { }

  ngOnInit() {
    this.formularioNuevaEmpresa = this.crearFormulario();
  }

  crearFormulario() {
    return this.formBuilder.group({
      nombreEmpresa: ["", Validators.required],
      nifEmpresa: ["", Validators.required]
    })
  }

  crearFormularioAdministrador() {
    return this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      nombre: ["", Validators.required],
      apellidos: ["", Validators.required],
      password: ["", Validators.required]
    })
  }

  cancelar() {
    this.router.navigateByUrl('');
  }

  async comprobarEmpresaDisponible() {

    let listaEmpresasFirebase = await this.firebaseFirestoreService.getListaEmpresas();

    console.log("Lista de empresas", listaEmpresasFirebase);

    if (listaEmpresasFirebase.filter(empresaFiltrada => empresaFiltrada.NIFEmpresa == this.formularioNuevaEmpresa.value.nifEmpresa).length > 0) {
      this.controlInformacionService.mostrarToast("Esta empresa ya está registrada", "danger");
    } else {
      this.formularioAdministrador = this.crearFormularioAdministrador();
      this.primerFormCorrecto = true;
    }

  }

  registrarAdminEmpresa() {
    let empresa: Empresa = {
      NIFEmpresa: this.formularioNuevaEmpresa.value.nifEmpresa,
      NombreEmpresa: this.formularioNuevaEmpresa.value.nombreEmpresa
    }
    this.firebaseFirestoreService.addEmpresa(empresa)
      .then((res) => {
        console.log("Empresa creada");
        let usuario: Usuario = {
          email: this.formularioAdministrador.value.email,
          departamento: {id:0,nombreDepartamento:"Administrador"},
          nombre: this.formularioAdministrador.value.nombre,
          password: this.formularioAdministrador.value.password,
          listaToken: []
        }
        this.firebaseAuthService.addUsuario(usuario)
          .then((res) => {

            this.firebaseFirestoreService.addUsuarioEmpresa(empresa, usuario)
              .then((res) => {
                this.controlInformacionService.mostrarToast("Usuario añadido", "success")
                this.router.navigateByUrl('');
              })
              .catch((err) => {
                this.controlInformacionService.mostrarToast("Error al añadir usuario", "danger")

              })

          })
          .catch((err) => {
            this.controlInformacionService.mostrarToast("Error al añadir usuario", "danger")

          })
      })
      .catch((err) => {
        this.controlInformacionService.mostrarToast("Error al crear empresa");
      })
  }
}
