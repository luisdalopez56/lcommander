import { ProcesosGlobalesService } from './../../Servicios/procesos-globales.service';
import { AppComponent } from './../../app.component';
import { Usuario } from './../../Modelos/Usuario';
import { FirebaseFirestoreService } from './../../Servicios/Firebase/firebase-firestore.service';
import { VariablesService } from './../../Servicios/variables.service';
import { ControlInformacionService } from './../../Servicios/control-informacion.service';
import { FirebaseAuthService } from './../../Servicios/Firebase/firebase-auth.service';
import { GlobalConf } from './../../Configuracion/GlobalConf';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FirebaseFCMService } from 'src/app/Servicios/Firebase/firebase-fcm.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginCorrecto: boolean = false;
  public globalConf = GlobalConf;

  public formularioLogin: FormGroup;
  public validaciones_mensajes_formulario = {
    'email': [
      { type: 'required', message: 'El email no puede estar vacío' },
      { type: 'email', message: 'El email no tiene un formato correcto' }
    ],
    'password': [
      { type: 'required', message: 'La contraseña no puede estar vacía' }
    ],

  }
  recordarUsuario: boolean;
  constructor(
    public formBuilder: FormBuilder,
    public firebaseAuth: FirebaseAuthService,
    public router: Router,
    public controlInformacionService: ControlInformacionService,
    public variables: VariablesService,
    public procesosGlobales: ProcesosGlobalesService,
    public menuCtrl: MenuController,
    public firestoreService: FirebaseFirestoreService,
    public appComponent: AppComponent,
    public firebaseFCM: FirebaseFCMService

  ) {
    this.formularioLogin = this.crearFormulario();
  }

  ngOnInit() {
  }

  async ionViewDidEnter(): Promise<void> {
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    await this.comprobarRecodarStorge();
  }

  crearFormulario() {
    return this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
      recordarusuario: [""]
    })
  }

  registrarNuevaEmpresa() {
    this.router.navigateByUrl('registrar-empresa');
  }

  async comprobarRecodarStorge() {
    let valueUsr, valuePwd, valueRecordar = null;

    await this.procesosGlobales.obtenerEnStorage('recordar')
      .then((res) => {
        valueRecordar = res;
        console.warn("Valor de recordar Storage: ", res);
      })

    if (valueRecordar == true) {
      //this.formularioLogin.value.recordarusuario = true;
      this.variables.recordarUsuario = true;
      // console.log('Got item recordar: ', valueRecordar.value, ' variables ', this.variables.recordarUsuario);

      await this.procesosGlobales.obtenerEnStorage('user')
        .then((res) => {
          valueUsr = res;
          //console.log("Valor de Usuario Storage: ", res);
        })

      if (valueUsr) {
        //this.formularioLogin.value.loginUsuario = valueUsr.value;

        this.formularioLogin.value.email = valueUsr
        //console.log('Got item: usuario ', valueUsr.value, " variables ", this.variables.usuario);
      }

      await this.procesosGlobales.obtenerEnStorage('pwd')
        .then((res) => {
          valuePwd = res;
          //console.log("Valor de Pwd Storage: ", res);
        })

      if (valuePwd) {
        //this.formularioLogin.value.passwordUsuario = valuePwd;
        this.formularioLogin.value.password = valuePwd;
        //console.log('Got item: password', valuePwd.value , ' variables ', this.variables.pwd)
      };
      this.recordarUsuario = true;
    } else {
      this.variables.recordarUsuario = false;
      this.formularioLogin.value.email = '';
      this.formularioLogin.value.password = '';
      this.recordarUsuario = false;
      //console.log("valueRecordar.value == 'false'", valuePwd.value , ' variables ', this.variables.pwd)

    }
    //console.log('Got item recordar final : ', valueRecordar.value, ' variables ', this.variables.recordarUsuario);
  }

  async cambiarRecordar() {
    this.recordarUsuario = !this.variables.recordarUsuario;
  }

  async login() {
    this.controlInformacionService.presentLoading("Iniciando sesión");
    await this.firebaseAuth.login(this.formularioLogin.value.email, this.formularioLogin.value.password)
      .then(async (res) => {
        console.log(res);

        if (res) {

          if (this.recordarUsuario) {
            //console.log("store recordar usuario ", this.recordarUsuario)
            await this.procesosGlobales.guardarEnStorage('user', JSON.stringify(this.formularioLogin.value.email));
            await this.procesosGlobales.guardarEnStorage('pwd', JSON.stringify(this.formularioLogin.value.password));
            await this.procesosGlobales.guardarEnStorage('recordar', JSON.stringify(this.variables.recordarUsuario));

          } else {
            await this.procesosGlobales.eliminarLocalStorage("user");
            await this.procesosGlobales.eliminarLocalStorage("pwd");
            await this.procesosGlobales.eliminarLocalStorage("recordar");
          }

          await this.firestoreService.getDatosEmpleado(this.formularioLogin.value.email)
            .then((empleado) => {
              this.variables.empleadoSesion = empleado;
              console.log(this.variables.empleadoSesion);

              this.firebaseFCM.firebaseMessaging();

            })

          this.appComponent.cargarCarta();
          this.appComponent.cargarComandas();
          this.appComponent.cargarTickets();

          // AppComponent.prototype.cargarCarta();
          this.router.navigateByUrl('dashboard')
          this.variables.loginCorrecto = true;
          this.menuCtrl.enable(true);
        }
        else {
          this.controlInformacionService.dismissLoading();
          // this.controlInformacionService.mostrarToast("")
        }
      })
      .catch(() => {
        this.controlInformacionService.dismissLoading();

      });

    this.controlInformacionService.dismissLoading();


    /* console.log(this.formularioLogin);

this.firebaseAuth.login(this.formularioLogin.value.email, this.formularioLogin.value.password) */
  }

}
