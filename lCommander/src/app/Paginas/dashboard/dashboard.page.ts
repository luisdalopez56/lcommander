import { AppComponent } from './../../app.component';
import { VariablesService } from './../../Servicios/variables.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  public dashboardMenus = [];

  public dashboardMostrar = [];

  constructor(
    public variables: VariablesService,
    public appComponent: AppComponent
  ) { }

  ngOnInit() {
    console.log("El empleado de la sesion es:", this.variables.empleadoSesion);
    this.comprobarPerfil();
    this.appComponent.cargarEmpleados();
  }

  comprobarPerfil() {

    if (this.variables.empleadoSesion.departamento.id == 0) { //ADMIN
      this.variables.appPages = this.variables.menuAdmin;
      this.dashboardMostrar = [
        { nombreOpcion: "Cocina", imagen: "../../assets/Imagenes/fogones.jpg", url: "/cocina" },
        { nombreOpcion: "Mesas", imagen: "../../assets/Imagenes/mesas.jpg", url: "/mesas" },
        { nombreOpcion: "Comandas", imagen: "../../assets/Imagenes/comandas.jpg", url: "/comandas" },
        { nombreOpcion: "Carta", imagen: "../../assets/Imagenes/carta.jpg", url: "/carta" },
        { nombreOpcion: "Camareros", imagen: "../../assets/Imagenes/camareros.jpg", url: "/camareros" },
        { nombreOpcion: "Empleados", imagen: "../../assets/Imagenes/empleados.jpg", url: "/empleados" },
        { nombreOpcion: "Tickets", imagen: "../../assets/Imagenes/tickets.jpg", url: "/tickets" }

      ];
    } else if (this.variables.empleadoSesion.departamento.id == 2) {
      this.variables.appPages = this.variables.menuCamareros;
      this.dashboardMostrar = [
        { nombreOpcion: "Camareros", imagen: "../../assets/Imagenes/camareros.jpg", url: "/camareros" },
        { nombreOpcion: "Mesas", imagen: "../../assets/Imagenes/mesas.jpg", url: "/mesas" },
        { nombreOpcion: "Comandas", imagen: "../../assets/Imagenes/comandas.jpg", url: "/comandas" },
        { nombreOpcion: "Tickets", imagen: "../../assets/Imagenes/tickets.jpg", url: "/tickets" },
        { nombreOpcion: "Carta", imagen: "../../assets/Imagenes/carta.jpg", url: "/carta" }
      ]
    } else if (this.variables.empleadoSesion.departamento.id == 1) {
      this.variables.appPages = this.variables.menuCocina;
      this.dashboardMostrar = [
        { nombreOpcion: "Cocina", imagen: "../../assets/Imagenes/fogones.jpg", url: "/cocina" },
        { nombreOpcion: "Comandas", imagen: "../../assets/Imagenes/comandas.jpg", url: "/comandas" },
        { nombreOpcion: "Carta", imagen: "../../assets/Imagenes/carta.jpg", url: "/carta" }
      ]
    }
  }

}

{
  empresas: [
    {
      empresa1: {
        nombreEmpresa:"Proyecto",
        empleados: [
          { nombre: "Paco",departamento:"Administrador" },
          { nombre: "Jesus",departamento:"Cocina" }
        ],
        carta: [
          { categoriaNombre: "Carne", platos: [{ nombrePlato: "Chuletón" }] },
          { categoriaNombre: "Pescado", platos: [{ nombrePlato: "Dorada" }] }
        ],
        comandas: [
          {fecha:"07/03/2022",mesa:"Mesa1",platos: [{ nombrePlato: "Dorada" }]},
          {fecha:"07/03/2022",mesa:"Mesa2",platos: [{ nombrePlato: "Agua" }]}],
        tickets: [
          {estado:"Cocinando",mesa:"Mesa1",platos: [{ nombrePlato: "Dorada" },{ nombrePlato: "Agua" }]}
        ],
        mesas: [
          {nombreMesa:"Mesa1"},
          {nombreMesa:"Mesa2"}
        ]
      }
    }
  ]
}
