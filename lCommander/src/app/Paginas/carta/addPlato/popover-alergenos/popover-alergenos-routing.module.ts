import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopoverAlergenosPage } from './popover-alergenos.page';

const routes: Routes = [
  {
    path: '',
    component: PopoverAlergenosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopoverAlergenosPageRoutingModule {}
