import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover-alergenos',
  templateUrl: './popover-alergenos.page.html',
  styleUrls: ['./popover-alergenos.page.scss'],
})
export class PopoverAlergenosPage implements OnInit {

  public alergenosActuales: string[];

  public listaAlergenos = [
    { nombre: "Altramuces", url: "../../../../../assets/Iconos/Alergenos/Altramuces.svg", seleccionado: false },
    { nombre: "Apio", url: "../../../../../assets/Iconos/Alergenos/Apio.svg", seleccionado: false },
    { nombre: "Azufre", url: "../../../../../assets/Iconos/Alergenos/Azufre.svg", seleccionado: false },
    { nombre: "Cacahuete", url: "../../../../../assets/Iconos/Alergenos/Cacahuete.svg", seleccionado: false },
    { nombre: "Crustaceo", url: "../../../../../assets/Iconos/Alergenos/Crustaceo.svg", seleccionado: false },
    { nombre: "Frutoscascara", url: "../../../../../assets/Iconos/Alergenos/Frutoscascara.svg", seleccionado: false },
    { nombre: "Gluten", url: "../../../../../assets/Iconos/Alergenos/Gluten.svg", seleccionado: false },
    { nombre: "Huevo", url: "../../../../../assets/Iconos/Alergenos/Huevo.svg", seleccionado: false },
    { nombre: "Lacteos", url: "../../../../../assets/Iconos/Alergenos/Lacteos.svg", seleccionado: false },
    { nombre: "Moluscos", url: "../../../../../assets/Iconos/Alergenos/Moluscos.svg", seleccionado: false },
    { nombre: "Mostaza", url: "../../../../../assets/Iconos/Alergenos/Mostaza.svg", seleccionado: false },
    { nombre: "Sesamo", url: "../../../../../assets/Iconos/Alergenos/Sesamo.svg", seleccionado: false },


  ];

  constructor(
    public navParams: NavParams,
    public popoverCtrl: PopoverController
  ) {
    this.alergenosActuales = this.navParams.data.alergenos;

    this.alergenosActuales.forEach((alergeno) => {
      this.listaAlergenos.filter(alergenoFiltrado => alergenoFiltrado.nombre == alergeno)[0].seleccionado = true;
    })
  }

  ngOnInit() {
    console.log(
      this.alergenosActuales
    );

  }

  seleccionarAlergeno(alergeno) {
    alergeno.seleccionado = !alergeno.seleccionado;
  }

  cerrarPopover() {
    let alergenosActivos = this.listaAlergenos.filter(alergenoFiltrado => alergenoFiltrado.seleccionado == true);

    this.popoverCtrl.dismiss(alergenosActivos);

  }

}
