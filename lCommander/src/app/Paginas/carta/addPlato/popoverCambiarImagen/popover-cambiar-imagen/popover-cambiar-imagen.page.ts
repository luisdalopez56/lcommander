import { Plato } from './../../../../../Modelos/Plato';
import { CategoriaPlatos } from './../../../../../Modelos/CategoriaPlatos';
import { VariablesService } from './../../../../../Servicios/variables.service';
import { FirebaseStorageService } from './../../../../../Servicios/Firebase/firebase-storage.service';
import { PopoverController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';

@Component({
  selector: 'app-popover-cambiar-imagen',
  templateUrl: './popover-cambiar-imagen.page.html',
  styleUrls: ['./popover-cambiar-imagen.page.scss'],
})
export class PopoverCambiarImagenPage implements OnInit {


  public listaImagenes: string[];

  constructor(
    public popoverCtrl: PopoverController,
    public fireStorage: FirebaseStorageService,
    public variables: VariablesService,
    public navParams: NavParams,
    public controlInformacion: ControlInformacionService
  ) {
    this.listaImagenes = [

      "../../../../../../assets/Iconos/ImagenPlato/burger.svg",
      "../../../../../../assets/Iconos/ImagenPlato/cocktail-drink-glass.svg",
      "../../../../../../assets/Iconos/ImagenPlato/dinner.svg",
      "../../../../../../assets/Iconos/ImagenPlato/fish.svg",
      "../../../../../../assets/Iconos/ImagenPlato/glass-bottle.svg",
      "../../../../../../assets/Iconos/ImagenPlato/hot-dog.svg",
      "../../../../../../assets/Iconos/ImagenPlato/hot-tea.svg",
      "../../../../../../assets/Iconos/ImagenPlato/ice-cream.svg",
      "../../../../../../assets/Iconos/ImagenPlato/meat-food.svg",
      "../../../../../../assets/Iconos/ImagenPlato/noodle.svg",
      "../../../../../../assets/Iconos/ImagenPlato/rice-bowl.svg",
      "../../../../../../assets/Iconos/ImagenPlato/sandwich.svg",
      "../../../../../../assets/Iconos/ImagenPlato/shrimp.svg",
      "../../../../../../assets/Iconos/ImagenPlato/soda-glass-bottle.svg",
      // "../../../../../../assets/Iconos/ImagenPlato/burger.svg",
      // "../../../../../../assets/Iconos/ImagenPlato/burger.svg",
      // "../../../../../../assets/Iconos/ImagenPlato/burger.svg",

    ]
  }

  ngOnInit() {
  }

  subirImagenSelect() {
    let input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');

    input.addEventListener('change', (event: any) => {
      let fileList: File[] = event.target.files;
      console.log("File List Object Value", fileList);

      let categoriaPlato: CategoriaPlatos = this.navParams.data.categoriaPlato;


      const reader = new FileReader();
      reader.onloadend = () => {
        // log to console
        // logs data:<type>;base64,wL2dvYWwgbW9yZ...
        console.log(reader.result);
        let nuevaImagen = {url: reader.result, file: fileList[0]};
        this.popoverCtrl.dismiss(nuevaImagen);
      };
      reader.readAsDataURL(fileList[0]);

      /*  this.fireStorage.subirImagenPlato(this.variables.empresaActual, fileList[0], categoriaPlato)
         .then((res) => {
           console.log(res);
           res.ref.getDownloadURL()
             .then((res) => {
               this.popoverCtrl.dismiss(res);
             })
             .catch((err) => {
               this.controlInformacion.mostrarToast("Error al subir archivo", "danger")
             })
         })
         .catch((err) => {
           console.log(err);

         }); */
    });

    input.click();
  }

  cerrarPopover() {
    this.popoverCtrl.dismiss();
  }

  imagenSeleccionada(imagen: string) {
    let nuevaImagen = {url: imagen, file: null};
    this.popoverCtrl.dismiss(nuevaImagen);
  }

}
