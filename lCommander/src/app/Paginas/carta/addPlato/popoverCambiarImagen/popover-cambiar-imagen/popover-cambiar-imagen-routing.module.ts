import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopoverCambiarImagenPage } from './popover-cambiar-imagen.page';

const routes: Routes = [
  {
    path: '',
    component: PopoverCambiarImagenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopoverCambiarImagenPageRoutingModule {}
