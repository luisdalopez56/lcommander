import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopoverCambiarImagenPageRoutingModule } from './popover-cambiar-imagen-routing.module';

import { PopoverCambiarImagenPage } from './popover-cambiar-imagen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverCambiarImagenPageRoutingModule
  ],
  declarations: [PopoverCambiarImagenPage]
})
export class PopoverCambiarImagenPageModule {}
