import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPlatoPageRoutingModule } from './add-plato-routing.module';

import { AddPlatoPage } from './add-plato.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    AddPlatoPageRoutingModule
  ],
  declarations: [AddPlatoPage]
})
export class AddPlatoPageModule { }
