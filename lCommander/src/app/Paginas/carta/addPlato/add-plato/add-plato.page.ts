import { FirebaseStorageService } from './../../../../Servicios/Firebase/firebase-storage.service';
import { PopoverCambiarImagenPage } from './../popoverCambiarImagen/popover-cambiar-imagen/popover-cambiar-imagen.page';
import { CategoriaPlatos } from './../../../../Modelos/CategoriaPlatos';
import { VariablesService } from './../../../../Servicios/variables.service';
import { FirebaseFirestoreService } from './../../../../Servicios/Firebase/firebase-firestore.service';
import { Plato } from './../../../../Modelos/Plato';
import { PopoverAlergenosPage } from './../popover-alergenos/popover-alergenos.page';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PopoverController, Platform, NavParams, ModalController, AlertController } from '@ionic/angular';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';

@Component({
  selector: 'app-add-plato',
  templateUrl: './add-plato.page.html',
  styleUrls: ['./add-plato.page.scss'],
})
export class AddPlatoPage implements OnInit {

  public formularioNuevoPlato: FormGroup
  public platoNuevo: Plato;
  public categoriaNuevoPlato: CategoriaPlatos;
  public imagenURL: { url: string, file: File } = { url: "", file: null };

  public validaciones_mensajes_formulario = {
    'nombrePlato': [
      { type: 'required', message: 'El nombre del plato no puede estar vacío' },
    ],
    'descripcion': [
      { type: 'required', message: 'La descripción no puede estar vacía' }
    ],
    'precio': [
      { type: 'required', message: 'El precio no puede estar vacío' }
    ],
  }
  detallePlato: boolean;

  constructor(
    public formBuilder: FormBuilder,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public firestoreService: FirebaseFirestoreService,
    public variables: VariablesService,
    public controlInformacion: ControlInformacionService,
    public fireStorage: FirebaseStorageService
  ) {
    this.formularioNuevoPlato = this.crearFormulario();

    this.categoriaNuevoPlato = this.navParams.data.categoria;
    this.detallePlato = false;

    if (this.navParams.data.platoSeleccionado) {
      this.platoNuevo = JSON.parse(JSON.stringify(this.navParams.data.platoSeleccionado));
      this.formularioNuevoPlato.setValue(
        {
          nombrePlato: this.platoNuevo.nombrePlato,
          descripcion: this.platoNuevo.descripcion,
          alergenos: this.platoNuevo.alergenos,
          precio: this.platoNuevo.precio,
        }
      )


      this.imagenURL.url = this.platoNuevo.imagenUrl;
      this.detallePlato = true;
    }else{
      this.platoNuevo = { id: "", nombrePlato: "", descripcion: "", alergenos: [], precio: 0, imagenUrl: "", disponible: true };
      this.imagenURL.url = "../../../../../assets/Iconos/ImagenPlato/dinner.svg";
    }
  }

  ngOnInit() {
    console.log(this.navParams.data.platoSeleccionado);


  }

  async marcarPlatoDisponible(plato: Plato, categoria: CategoriaPlatos){
    let alert = await this.alertCtrl.create({
      message: "¿Marcar como disponible?",
      header: "Marcar disponible",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Disponible",
          handler: () => {

            plato.disponible = true;
            let indexPlato = categoria.platos.findIndex(platoFiltrado => platoFiltrado.id == plato.id);
            categoria.platos[indexPlato] = plato;

            this.firestoreService.addPlatoNuevo(this.variables.empresaActual, categoria)
              .then(() => {
                this.controlInformacion.mostrarToast("Plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
                console.error("Error al borrar archivo", err);
              })
          }
        }

      ]
    })

    alert.present();
  }

  async marcarPlatoNoDisponible(plato: Plato, categoria: CategoriaPlatos) {
    let alert = await this.alertCtrl.create({
      message: "¿Marcar como no disponible?",
      header: "No disponible",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "No disponible",
          handler: () => {

            plato.disponible = false;
            let indexPlato = categoria.platos.findIndex(platoFiltrado => platoFiltrado.id == plato.id);
            categoria.platos[indexPlato] = plato;

            this.firestoreService.addPlatoNuevo(this.variables.empresaActual, categoria)
              .then(() => {
                this.controlInformacion.mostrarToast("Plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
                console.error("Error al borrar archivo", err);
              })
          }
        }

      ]
    })

    alert.present();
  }

  platoCambiado() {
    this.platoNuevo.nombrePlato = this.formularioNuevoPlato.value.nombrePlato;
    this.platoNuevo.descripcion = this.formularioNuevoPlato.value.descripcion;
    this.platoNuevo.precio = this.formularioNuevoPlato.value.precio;
    this.platoNuevo.imagenUrl = this.imagenURL.url;
    // this.platoNuevo.disponible = true;

    return JSON.stringify(this.navParams.data.platoSeleccionado) == JSON.stringify(this.platoNuevo);
  }

  crearFormulario() {
    return this.formBuilder.group({
      nombrePlato: ["", Validators.required],
      descripcion: ["", Validators.required],
      alergenos: [[]],
      precio: ["", [Validators.required, Validators.min(0.1)]],
    })
  }

  async editarPlato() {

    if (this.imagenURL.url != this.navParams.data.platoSeleccionado.imagenUrl) {
      console.log("Entra aqui");
      console.log(this.imagenURL.url);

      if (this.imagenURL.url.includes("base64")) {
        console.log("Imnagen subida nueva");

        if (this.navParams.data.platoSeleccionado.imagenUrl.includes("https")) {
          console.log("Antes tenia imagen subida");

          await this.fireStorage.borrarImagen(this.variables.empresaActual.NIFEmpresa + "/" + this.categoriaNuevoPlato.nombreCategoria + "/" + this.platoNuevo.id)
            .toPromise()
            .then(() => {
              console.log("Archivo anterior borrado");
            })
            .catch((err) => {
              console.log("El archivo ya no existe");
            })
        }



        await this.fireStorage.subirImagenPlato(this.variables.empresaActual, this.imagenURL.file, this.categoriaNuevoPlato, this.platoNuevo)
          .then(async (res) => {
            console.log("Imagen subida");
            await res.ref.getDownloadURL()
              .then((url) => {
                this.platoNuevo.imagenUrl = url;
                let indexPlato = this.categoriaNuevoPlato.platos.findIndex(platoFiltrado => platoFiltrado.id == this.platoNuevo.id);
                this.categoriaNuevoPlato.platos[indexPlato] = this.platoNuevo;

                // this.modalCtrl.dismiss(res);
                this.firestoreService.addPlatoNuevo(this.variables.empresaActual, this.categoriaNuevoPlato)
                  .then((res) => {
                    console.log("Nuevo plato añadido");
                    this.modalCtrl.dismiss(res);
                    this.controlInformacion.mostrarToast("Plato editado", "success");
                  })
                  .catch((err) => {
                    this.controlInformacion.mostrarToast("Error al editar plato", "danger");
                  })
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al subir archivo", "danger")
              })
          })
          .catch((err) => {
            console.log(err);

          });

      } else {
        console.log("Imagen prediseñada");

        if (this.navParams.data.platoSeleccionado.imagenUrl.includes("https")) {
          console.log("Antes tenia imagen subida");

          await this.fireStorage.borrarImagen(this.variables.empresaActual.NIFEmpresa + "/" + this.categoriaNuevoPlato.nombreCategoria + "/" + this.platoNuevo.id)
            .toPromise()
            .then(() => {
              console.log("Archivo anterior borrado");
            })
            .catch((err) => {
              console.log("El archivo ya no existe");
            })
        }

        console.log("Imagen a actualizar: ", this.imagenURL);


        this.platoNuevo.imagenUrl = this.imagenURL.url;

        let indexPlato = this.categoriaNuevoPlato.platos.findIndex(platoFiltrado => platoFiltrado.id == this.platoNuevo.id);
        this.categoriaNuevoPlato.platos[indexPlato] = this.platoNuevo;

        console.log(this.categoriaNuevoPlato);

        await this.firestoreService.addPlatoNuevo(this.variables.empresaActual, this.categoriaNuevoPlato)
          .then((res) => {
            this.modalCtrl.dismiss();
            this.controlInformacion.mostrarToast("Plato editado", "success");
          })
          .catch((err) => {
            this.controlInformacion.mostrarToast("Error al editar plato", "danger");
          })
      }
    } else {
      let indexPlato = this.categoriaNuevoPlato.platos.findIndex(platoFiltrado => platoFiltrado.id == this.platoNuevo.id);
      this.categoriaNuevoPlato.platos[indexPlato] = this.platoNuevo;

      console.log(this.categoriaNuevoPlato);

      await this.firestoreService.addPlatoNuevo(this.variables.empresaActual, this.categoriaNuevoPlato)
        .then((res) => {
          this.modalCtrl.dismiss();
          this.controlInformacion.mostrarToast("Plato editado", "success");
        })
        .catch((err) => {
          this.controlInformacion.mostrarToast("Error al editar plato", "danger");
        })
    }

  }

  async cambiarImagen() {
if(this.variables.empleadoSesion.departamento.id == 0){
  let popover = await this.popoverCtrl.create({
    component: PopoverCambiarImagenPage,
    cssClass: "cambiarImagenPopover",
    componentProps: { urlImagen: this.imagenURL, categoriaPlato: this.categoriaNuevoPlato, plato: this.platoNuevo }
  })

  popover.present();

  popover.onDidDismiss()
    .then((res) => {
      let nuevaUrl = res.data;
      if (nuevaUrl) {
        // if(this.imagenURL)
        this.imagenURL = nuevaUrl;
      };
    })
}

  }

  async abrirPopoverAlergenos() {

    let css = "popoverAddPlato";
    if (!this.platform.is("desktop")) css = "popoverAddPlatoMobile";

    let popover = await this.popoverCtrl.create({
      component: PopoverAlergenosPage,
      componentProps: { alergenos: this.platoNuevo.alergenos },
      cssClass: css,
    })

    popover.present();

    popover.onDidDismiss().then((res) => {
      console.log(res);
      let arrayAlergenos: any[] = res.data;
      this.platoNuevo.alergenos = [];

      if (arrayAlergenos.length > 0) {
        arrayAlergenos.forEach(alergeno => {
          this.platoNuevo.alergenos.push(alergeno.nombre);
        })
      }
    })
  }

  async crearPlato() {
    this.platoNuevo.nombrePlato = this.formularioNuevoPlato.value.nombrePlato;
    this.platoNuevo.descripcion = this.formularioNuevoPlato.value.descripcion;
    this.platoNuevo.precio = this.formularioNuevoPlato.value.precio;
    this.platoNuevo.disponible = true;
    this.platoNuevo.id = this.platoNuevo.nombrePlato.replace(/\s/g, '');
    this.categoriaNuevoPlato.platos.push(this.platoNuevo);

    console.log(this.imagenURL);


    if (this.imagenURL.url.includes("base64")) {


      await this.fireStorage.subirImagenPlato(this.variables.empresaActual, this.imagenURL.file, this.categoriaNuevoPlato, this.platoNuevo)
        .then(async (res) => {
          console.log(res);
          await res.ref.getDownloadURL()
            .then((url) => {
              this.platoNuevo.imagenUrl = url;
              // this.modalCtrl.dismiss(res);
            })
            .catch((err) => {
              this.controlInformacion.mostrarToast("Error al subir archivo", "danger")
            })
        })
        .catch((err) => {
          console.log(err);

        });
    } else this.platoNuevo.imagenUrl = this.imagenURL.url;


    this.firestoreService.addPlatoNuevo(this.variables.empresaActual, this.categoriaNuevoPlato)
      .then((res) => {
        console.log("Nuevo plato añadido");
        this.modalCtrl.dismiss(res);
        this.controlInformacion.mostrarToast("Plato añadido", "success");

      })
      .catch((err) => {
        this.controlInformacion.mostrarToast("Error al añadir plato", "danger");
      })

  }

  cerrarModal() {
    this.modalCtrl.dismiss(this.platoNuevo);
  }

}
