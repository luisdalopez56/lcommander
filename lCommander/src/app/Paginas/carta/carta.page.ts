import { Plato } from './../../Modelos/Plato';
import { AddPlatoPage } from './addPlato/add-plato/add-plato.page';
import { CategoriaPlatos } from './../../Modelos/CategoriaPlatos';
import { VariablesService } from './../../Servicios/variables.service';
import { FirebaseFirestoreService } from './../../Servicios/Firebase/firebase-firestore.service';
import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';
import { title } from 'process';

@Component({
  selector: 'app-carta',
  templateUrl: './carta.page.html',
  styleUrls: ['./carta.page.scss'],
})
export class CartaPage implements OnInit {

  constructor(
    public firestoreService: FirebaseFirestoreService,
    public variables: VariablesService,
    public alertCtrl: AlertController,
    public controlInformacion: ControlInformacionService,
    public modalCtrl: ModalController,

  ) {
  }

  ngOnInit() {
  }



  async addNuevaCategoria() {
    let alert = await this.alertCtrl.create({
      header: "Nueva categoría",
      message: "Añadir nueva categoría",
      inputs: [{
        placeholder: "Nombre de la categoría",
        label: "Categoría",
        name: "category"
      }],
      buttons: [
        {
          text: "Crear",
          handler: (datos) => {

            if (this.variables.carta.filter(categoria => categoria.nombreCategoria.toLowerCase() == datos.category.toLowerCase()).length == 0) {
              let categoria: CategoriaPlatos = { nombreCategoria: titleCaseWord(datos.category), platos: [] };

              function titleCaseWord(word: string) {
                if (!word) return word;
                return word[0].toUpperCase() + word.substr(1).toLowerCase();
              }

              this.firestoreService.addCategoriaCarta(this.variables.empresaActual, categoria)
                .then(() => {
                  console.log("Añadida categoria");
                })
                .catch((err) => {
                  this.controlInformacion.mostrarToast("Error al añadir categoría", "danger");
                  return false
                })
            } else {
              this.controlInformacion.mostrarToast("La categoría ya está en la carta", "danger");
              return false
            }

          }
        },
        {
          text: "Cancelar",
          role: "cancel"
        }
      ]
    })

    alert.present();
  }

  async verPlato(platoSeleccionado: Plato, categoriaSeleccionada: CategoriaPlatos) {
    let modal = await this.modalCtrl.create({
      component: AddPlatoPage,
      componentProps: { platoSeleccionado: platoSeleccionado, categoria: categoriaSeleccionada }
    })

    modal.present();
  }

  async eliminarPlato(plato: Plato, categoria: CategoriaPlatos) {
    let alert = await this.alertCtrl.create({
      message: "¿Seguro que quieres eliminar este plato de la carta?",
      header: "Eliminar plato",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Eliminar",
          handler: () => {
            this.firestoreService.deletePlato(this.variables.empresaActual, categoria, plato)
              .then(() => {
                this.controlInformacion.mostrarToast("Plato eliminado de la carta", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al eliminar plato", "danger");
                console.error("Error al borrar archivo", err);
              })
          }
        }

      ]
    })

    alert.present();
  }

  async marcarPlatoNoDisponible(plato: Plato, categoria: CategoriaPlatos) {
    let alert = await this.alertCtrl.create({
      message: "¿Marcar como no disponible?",
      header: "No disponible",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "No disponible",
          handler: () => {

            plato.disponible = false;
            let indexPlato = categoria.platos.findIndex(platoFiltrado => platoFiltrado.id == plato.id);
            categoria.platos[indexPlato] = plato;

            this.firestoreService.addPlatoNuevo(this.variables.empresaActual, categoria)
              .then(() => {
                this.controlInformacion.mostrarToast("Plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
                console.error("Error al borrar archivo", err);
              })
          }
        }

      ]
    })

    alert.present();
  }

  async marcarPlatoDisponible(plato: Plato, categoria: CategoriaPlatos){
    let alert = await this.alertCtrl.create({
      message: "¿Marcar como disponible?",
      header: "Marcar disponible",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Disponible",
          handler: () => {

            plato.disponible = true;
            let indexPlato = categoria.platos.findIndex(platoFiltrado => platoFiltrado.id == plato.id);
            categoria.platos[indexPlato] = plato;

            this.firestoreService.addPlatoNuevo(this.variables.empresaActual, categoria)
              .then(() => {
                this.controlInformacion.mostrarToast("Plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
                console.error("Error al borrar archivo", err);
              })
          }
        }

      ]
    })

    alert.present();
  }

  async addNuevoPlato(categoria: CategoriaPlatos) {


    let modal = await this.modalCtrl.create({
      component: AddPlatoPage,
      componentProps: { categoria: categoria }
    })

    modal.present();
  }

}
