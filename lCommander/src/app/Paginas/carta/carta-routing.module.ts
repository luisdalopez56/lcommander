import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartaPage } from './carta.page';

const routes: Routes = [
  {
    path: '',
    component: CartaPage
  },
  {
    path: 'add-plato',
    loadChildren: () => import('./addPlato/add-plato/add-plato.module').then( m => m.AddPlatoPageModule)
  },  {
    path: 'popover-alergenos',
    loadChildren: () => import('./addPlato/popover-alergenos/popover-alergenos.module').then( m => m.PopoverAlergenosPageModule)
  },
  {
    path: 'popover-cambiar-imagen',
    loadChildren: () => import('./addPlato/popoverCambiarImagen/popover-cambiar-imagen/popover-cambiar-imagen.module').then( m => m.PopoverCambiarImagenPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartaPageRoutingModule {}
