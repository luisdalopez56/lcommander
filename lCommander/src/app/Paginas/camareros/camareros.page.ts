import { VariablesService } from './../../Servicios/variables.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { FirebaseFirestoreService } from 'src/app/Servicios/Firebase/firebase-firestore.service';
import { ControlInformacionService } from 'src/app/Servicios/control-informacion.service';
import * as moment from 'moment';
import { Comanda, PlatoComanda } from 'src/app/Modelos/Comanda';

@Component({
  selector: 'app-camareros',
  templateUrl: './camareros.page.html',
  styleUrls: ['./camareros.page.scss'],
})
export class CamarerosPage implements OnInit {

  moment: any = moment;
  fechaHoy = moment().format("DD/MM/YYYY mm:ss");
  comandaPulsada: string;
  platoPulsado: number;

  comandaPulsadaPreparado: string;
  platoPulsadoPreparado: number;

  constructor(
    public variables: VariablesService,
    public alertCtrl: AlertController,
    public firestoreService: FirebaseFirestoreService,
    public controlInformacion: ControlInformacionService
  ) { }

  ngOnInit() {
  }

  diff_minutes(dt1) {
    var time = new Date().getTime() - new Date(dt1).getTime();
    // console.log(time);

    function msToTime(duration) {
      // var milliseconds = Math.floor((duration % 1000) / 100);
      let seconds = Math.floor((duration / 1000) % 60);
       let minutes = Math.floor((duration / (1000 * 60)) % 60);
       let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

     let hoursF = (hours < 10) ? "0" + hours : hours;
     let minutesF = (minutes < 10) ? "0" + minutes : minutes;
     let secondsF = (seconds < 10) ? "0" + seconds : seconds;

     let textFinal ="";
      if(hoursF > 0) textFinal = textFinal + hoursF + "hor ";
      textFinal = textFinal + minutesF + "min";

      return textFinal;
    }

    return msToTime(time);
  }

  comandasPendientes(): Comanda[] {
    // this.variables.comandas = this.variables.comandas.sort((a, b) => (a.fechaComanda > b.fechaComanda) ? 1 : -1);
    return this.variables.comandas.filter(comandaFiltro => comandaFiltro.estado == 0);
  }

  comandasListas(): Comanda[] {
    let comandasTotales = [];
    this.variables.comandas = this.variables.comandas.sort((a, b) => (a.fechaComanda > b.fechaComanda) ? 1 : -1);
    let comandasSinTerminar = this.variables.comandas.filter(comandaFiltro => comandaFiltro.estado == 0);
    comandasSinTerminar.forEach(comanda => {
      comanda.platos = comanda.platos.filter(platoFilter => platoFilter.estado == 2)
      comandasTotales.push(comanda)
    }
    )
    return comandasTotales;
  }


  platosPreparando(platos: PlatoComanda[]) {
    // let platosPentientes = platos.sort((a, b) => (a.nombrePlato > b.nombrePlato) ? 1 : -1);
    return platos.filter(platoFiltro => platoFiltro.estado == 1 || platoFiltro.estado == 0);
  }

  platosListos(platos: PlatoComanda[]) {
    // let platosPentientes = platos.sort((a, b) => (a.nombrePlato > b.nombrePlato) ? 1 : -1);
    return platos.filter(platoFiltro => platoFiltro.estado == 2);
  }

  platosListosEnAlgunaComanda() {
    let hayPlatosListos = false;
    let comandasListas = this.variables.comandas.filter(comandaFiltro => comandaFiltro.estado == 0);
    comandasListas.every(comanda => {
      if (comanda.platos.filter(platoFiltrado => platoFiltrado.estado == 2).length > 0) {
        hayPlatosListos = true;
        return true;
      }


    })
    return hayPlatosListos;

  }

  platoPulsadoComandaPreparada(comanda: Comanda, numeroPlato: number) {
    if (this.comandaPulsadaPreparado == comanda.id && numeroPlato == this.platoPulsadoPreparado) {
      this.comandaPulsadaPreparado = null;
      this.platoPulsadoPreparado = null;
    }
    else {
      this.comandaPulsadaPreparado = comanda.id;
      this.platoPulsadoPreparado = numeroPlato;
    }
  }

  platoPulsadoComanda(comanda: Comanda, numeroPlato: number) {
    if (this.comandaPulsada == comanda.id && numeroPlato == this.platoPulsado) {
      this.comandaPulsada = null;
      this.platoPulsado = null;
    }
    else {
      this.comandaPulsada = comanda.id;
      this.platoPulsado = numeroPlato;
    }
  }

  async marcarPlatoEntregado(plato: PlatoComanda, comanda: Comanda, index: number) {
    let alert = await this.alertCtrl.create({
      header: "¿Plato entregado?",
      message: "Vas a marcar que este plato se ha entregado al cliente, ¿estás seguro?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].estado = 3;
            if(this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos.every(platoFiltrado => platoFiltrado.estado == 3)){
              this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].estado=1;
            }
            // this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].usuarioCocina = this.variables.empleadoSesion;
            this.firestoreService.addComanda(this.variables.empresaActual, this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0])
              .then(() => {
                this.controlInformacion.mostrarToast("Estado del plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
              })
          }
        }
      ]
    })

    alert.present();
  }


  async marcarPlatoPreparando(plato: PlatoComanda, comanda: Comanda, index: number) {
    let alert = await this.alertCtrl.create({
      header: "¿Preparando plato?",
      message: "Estas apunto de poner este plato en preparación, ¿estás seguro?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].estado = 1;
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].usuarioCocina = this.variables.empleadoSesion;
            this.firestoreService.addComanda(this.variables.empresaActual, this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0])
              .then(() => {
                this.controlInformacion.mostrarToast("Estado del plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
              })
          }
        }
      ]
    })

    alert.present();
  }

  async marcarPlatoLlevarAMesa(plato: PlatoComanda, comanda: Comanda, index: number) {
    let alert = await this.alertCtrl.create({
      header: "¿Plato listo?",
      message: "Estas apunto de poner este plato para entregar al cliente, ¿estás seguro?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].estado = 2;
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].usuarioCocina = this.variables.empleadoSesion;
            this.firestoreService.addComanda(this.variables.empresaActual, this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0])
              .then(() => {
                this.controlInformacion.mostrarToast("Estado del plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
              })
          }
        }
      ]
    })

    alert.present();
  }

  async marcarPlatoEnEspera(plato: PlatoComanda, comanda: Comanda, index: number) {
    let alert = await this.alertCtrl.create({
      header: "¿Plato en espera?",
      message: "Estas apunto de poner este plato en espera de nuevo, ¿estás seguro?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].estado = 0;
            this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0].platos[index].usuarioCocina = null;
            this.firestoreService.addComanda(this.variables.empresaActual, this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.id == comanda.id)[0])
              .then(() => {
                this.controlInformacion.mostrarToast("Estado del plato modificado", "success");
              })
              .catch((err) => {
                this.controlInformacion.mostrarToast("Error al modificar plato", "danger");
              })
          }
        }

      ]
    })

    alert.present();
  }

}



