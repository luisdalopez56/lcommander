import { Comanda } from './../../Modelos/Comanda';
import { VariablesService } from './../../Servicios/variables.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comandas',
  templateUrl: './comandas.page.html',
  styleUrls: ['./comandas.page.scss'],
})
export class ComandasPage implements OnInit {

  comandasCopia: Comanda[];
  filtrar: string = "0";

  constructor(
    public variables: VariablesService
  ) {
    this.variables.comandas = this.variables.comandas.sort((a, b) => (a.fechaComanda > b.fechaComanda) ? 1 : -1);
  }

  ngOnInit() {
  }

  filtrarComandas(): Comanda[] {

    let comandas: Comanda[];

    switch (this.filtrar) {
      case "0":
        comandas = this.variables.comandas.filter(comandaFiltrada => comandaFiltrada.estado == 0)

        break;
      case "1":
        comandas = this.variables.comandas;

        break;

      default:
        break;
    }

    return comandas;
  }



}
