import { CategoriaPlatos } from './../../Modelos/CategoriaPlatos';
import { Plato } from './../../Modelos/Plato';
import { Empresa } from 'src/app/Modelos/Empresa';
import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';

@Injectable({
  providedIn: 'root'
})
export class FirebaseStorageService {

  constructor(
    public fireStorage: AngularFireStorage
  ) { }

  subirImagenPlato(empresa: Empresa, archivo: File, categoriaPlato: CategoriaPlatos, platoNuevo: Plato) {
    console.log(archivo);

    return this.fireStorage.upload(empresa.NIFEmpresa + "/" + categoriaPlato.nombreCategoria + "/" + platoNuevo.id, archivo,{contentType: archivo.type});
  }

  borrarImagen(url: string){
    return this.fireStorage.ref(url).delete()
  }
}
