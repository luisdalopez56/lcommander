import { getAuth } from '@angular/fire/auth';
import { VariablesService } from './../variables.service';
import { Usuario } from './../../Modelos/Usuario';
import { ControlInformacionService } from './../control-informacion.service';
import { Injectable } from '@angular/core';
import { FirebaseApp } from '@angular/fire/app';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import * as firebase from 'firebase/app';
import { signOut } from 'firebase/auth';

@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {

  constructor(
    public firebaseAuth: AngularFireAuth,
    public controlInformacion: ControlInformacionService,
    public variables: VariablesService
  ) {

  }

  async login(usuario: string, pwd: string): Promise<boolean> {
    return this.firebaseAuth.signInWithEmailAndPassword(usuario, pwd)
      .then((res) => {
        return true;
      })
      .catch((err) => {

        this.controlInformacion.mostrarToast("Error al iniciar sesión, compruebe las credenciales", "danger");
        return false;
      });
  }

  addUsuario(usuario: Usuario) {
    return this.firebaseAuth.createUserWithEmailAndPassword(usuario.email, usuario.password);
  }

  deleteUsuario(usuario: Usuario) {
    console.log("usuario para borrar", usuario);

    const auth = getAuth();

    signOut(auth).then(() => {
      getAuth().signOut().then(() => {
        console.log("Cerrada sesión actual");
        this.firebaseAuth.signInWithEmailAndPassword(usuario.email, usuario.password)
          .then((res) => {
            res.user.reload();
            res.user.delete()
              .then(async () => {
                this.controlInformacion.mostrarToast("Usuario eliminado", "success");
                await this.firebaseAuth.signInWithEmailAndPassword(this.variables.empleadoSesion.email, this.variables.empleadoSesion.password)
              });

          })
      });
    })

  }

  modificarPwd(usuario: Usuario) {
    this.firebaseAuth.currentUser
      .then((res) => { res.updatePassword(usuario.password) })
      .catch((err) => {
        this.controlInformacion.mostrarToast("Error al actualizar contraseña", "danger");
      })
  }


}
