import { GlobalConf } from './../../Configuracion/GlobalConf';
import { Usuario } from './../../Modelos/Usuario';
import { FirebaseFirestoreService } from './firebase-firestore.service';
import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { PushNotification, PushNotifications, PushNotificationToken, Token } from '@capacitor/push-notifications';
import { LocalNotifications } from '@capacitor/local-notifications';

import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import { mergeMapTo, tap } from 'rxjs/operators';
import { Platform } from '@ionic/angular';
import { VariablesService } from '../variables.service';
import { TokenFCM } from 'src/app/Modelos/Usuario';
import { Device } from '@capacitor/device';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class FirebaseFCMService {

  constructor(
    public afMessaging: AngularFireMessaging,
    public platform: Platform,
    public variables: VariablesService,
    public firestoreService: FirebaseFirestoreService,
    public http: HttpClient,
  ) { }

  async firebaseMessaging() {

    if (!this.platform.is('desktop') && !this.platform.is('mobileweb') && !this.platform.is('pwa')) {
      console.log('Inicializo notificaciones FCM.');
      const info = await Device.getInfo();
      console.log(info.platform)

      this.afMessaging.requestToken.subscribe(async (token) => {
        console.log("El token dispositivo: ", token);

        this.variables.tokenFCMDispositivo = token;

        if (!this.variables.empleadoSesion.listaToken) this.variables.empleadoSesion.listaToken = [];


        if (this.variables.empleadoSesion.listaToken.some(e => e.token == this.variables.tokenFCMDispositivo)) console.log("Dispositivo registrado");
        else {
          const info = await Device.getInfo();
          let token: TokenFCM = { token: this.variables.tokenFCMDispositivo, plataforma: info.platform }
          this.variables.empleadoSesion.listaToken.push(token);
          this.firestoreService.updateUsuarioEmpresa(this.variables.empresaActual, this.variables.empleadoSesion);
        }

        this.iniciarServicioFCMDelDispositivo();
      });

    } else if (this.platform.is('desktop') || this.platform.is('mobileweb') || this.platform.is('pwa')) {
      console.log('Inicializo notificaciones PWA.');

      this.afMessaging.requestToken.subscribe(async (token) => {
        console.log("El token PWA: ", token);

        this.variables.tokenFCMDispositivo = token;

        if (!this.variables.empleadoSesion.listaToken) this.variables.empleadoSesion.listaToken = [];


        if (this.variables.empleadoSesion.listaToken.some(e => e.token == this.variables.tokenFCMDispositivo)) console.log("Dispositivo registrado");
        else {
          const info = await Device.getInfo();
          let token: TokenFCM = { token: this.variables.tokenFCMDispositivo, plataforma: info.platform }
          this.variables.empleadoSesion.listaToken.push(token);
          this.firestoreService.updateUsuarioEmpresa(this.variables.empresaActual, this.variables.empleadoSesion);
        }

        this.iniciarNotificacionesPushPWA();
      });
    }

  }

  getNotificacionesForegroundPWA() {
    return this.afMessaging.messages;
  }

  async iniciarNotificacionesPushPWA() {

    //Observable de notificaciones en primer plano, cuando recibe una notificación la recuperamos y dependiendo de la plataforma la mostramos de una forma o de otra
    this.getNotificacionesForegroundPWA().subscribe(async (msg: any) => {
      let notificacion = JSON.parse(msg.data.notification)

      console.log({ notificacion });

      if (this.platform.is('desktop')) {
        console.log('Muestro notificación PWA en desktop', notificacion);
        var notification = new Notification(notificacion.title, {
           body: notificacion.body, 
           icon: '../../assets/Icono.png',
        })
        //Evento para controlar cuando el usuario pulsa la notificación
        notification.onclick = function (event) {
          //console.log('Pulso notificación');
        }
      } else {
        console.log('Muestro notificación PWA en dispositivo');
        Notification.requestPermission(function (result) {
          if (result === 'granted') {
            navigator.serviceWorker.ready.then(function (registration) {
              registration.showNotification(
                notificacion.title, {
                body: notificacion.body,
                icon: '../../assets/Icono.png',
                vibrate: [200],
                //tag: 'vibration-sample'
              });
            });
          }
        });

      }

    });
  }

  // SE PIDEN LOS PERMISOS PARA EL DISPOSITIVO, SE REGISTRA EL TOKEN Y SE INICIA EL ESCUCHADOR PARA LAS NOTIFICACIONES EN PRIMER PLANO
  async iniciarServicioFCMDelDispositivo() {
    PushNotifications.requestPermissions().then(async result => {
      if (result.receive == 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        console.log("Permisos dados para FCM");

        PushNotifications.register();
        await PushNotifications.addListener('registration', (token: PushNotificationToken) => {// DEVUELVE EL TOKEN FCM
          this.variables.tokenFCMDispositivo = token.value;
          console.log(this.platform.platforms);

          if ((this.platform.is("ios") || this.platform.is("android")) && !this.platform.is("mobileweb")) {

            PushNotifications.addListener('pushNotificationReceived',
              (notification: PushNotification) => {
                console.log('Push received: ' + JSON.stringify(notification));
                LocalNotifications.schedule({
                  notifications: [{
                    id: 1,
                    title: notification.title,
                    body: notification.body,
                    attachments: [notification.data.image],// + notificacion.data[0] + notificacion.data.empleado,
                  }]
                })

              }
            );

            // Method called when tapping on a notification
            /* PushNotifications.addListener('pushNotificationActionPerformed',
              (notification: PushNotificationActionPerformed) => {
                console.log('Push action performed: ' + JSON.stringify(notification));
              }
            ); */
          }
        });
      } else {
        console.error("No hay permisos para FCM")
      }
    });

  }

  async enviarNotificacion(usuario: Usuario, titulo: string, mensaje: string) {

    if (usuario.listaToken) {
      usuario.listaToken.forEach(token => {
        let data;
        if (token.plataforma == 'web') {
          data = {
            "data": {
              "notification": {
                "title": titulo,
                "body": mensaje,
              },
            },
            "to": token.token,
          }
        } else {
          data = {
            "notification": {
              "title": titulo,
              "body": mensaje,
            },
            "data": {
              //"dat": dat,
            },
            "apns": {
              "headers": {
                "apns-priority": "5"
              }
            },
            "to": token.token,
          }
        }
        let options = {
          headers: {
            'Authorization': GlobalConf.claveServidorFCM
          }
        };
        this.http.post("https://fcm.googleapis.com/fcm/send", data, options).subscribe((respuesta) => {
          console.log("Mandada notificacion a: ", token.token, ' res: ', respuesta);
        }, err => {
          console.error("Error al enviar notificacion al dispositivo, ", data.to + "Error: ", err);
        });
      });
    }
  }
}
