import { Ticket } from './../../Modelos/Ticket';
import { Mesa } from './../../Modelos/Mesa';
import { Estancias } from './../../Modelos/Estancias';
import { Plato } from './../../Modelos/Plato';
import { Empresa } from './../../Modelos/Empresa';
import { CategoriaPlatos } from './../../Modelos/CategoriaPlatos';
import { VariablesService } from './../variables.service';
import { Usuario } from './../../Modelos/Usuario';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Comanda } from 'src/app/Modelos/Comanda';

@Injectable({
  providedIn: 'root'
})
export class FirebaseFirestoreService {

  public cartaCollection: AngularFirestoreCollection<CategoriaPlatos>;
  public carta: Observable<CategoriaPlatos[]>;


  constructor(
    public firebaseFirestore: AngularFirestore,
    public variables: VariablesService
  ) { }

  async getListaEmpresas(): Promise<Empresa[]> {
    const empresas = await this.firebaseFirestore.collection<Empresa>('empresas').get().toPromise();
    return empresas.docs.map(doc => doc.data());
  }

  addEmpresa(empresa: Empresa) {
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).set(empresa);
  }

  addPlatoNuevo(empresa: Empresa, categoria: CategoriaPlatos) {
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('carta').doc(categoria.nombreCategoria).set(categoria);
  }

  deletePlato(empresa: Empresa, categoria: CategoriaPlatos, plato: Plato) {

    let indexPlato = categoria.platos.findIndex(platoFiltrado => platoFiltrado.id == plato.id);
    categoria.platos.splice(indexPlato, 1);

    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('carta').doc(categoria.nombreCategoria).set(categoria);
  }

  addUsuarioEmpresa(empresa: Empresa, usuario: Usuario) {
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('empleados').add(usuario);
  }

  updateUsuarioEmpresa(empresa: Empresa, usuario: Usuario) {
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('empleados').doc(usuario.id).set(usuario);
  }

  async getDatosEmpleado(email: string): Promise<Usuario> {
    let usuarioLogin: Usuario;

    await this.getListaEmpresas()
      .then(async (res) => {
        for (let index = 0; index < res.length; index++) {
          const empresa = res[index];
          await this.firebaseFirestore.collection<Empresa>('empresas').doc(empresa.NIFEmpresa)
            .collection<Usuario>('empleados').ref.where("email", "==", email).get()
            .then((empleado) => {

              empleado.docs.forEach((doc) => {
                usuarioLogin = doc.data() as Usuario;
                usuarioLogin.id = doc.id;
              })

              if (!empleado.empty) {
                index = res.length
                this.variables.empresaActual = empresa;
              };

              return usuarioLogin;

            })
            .catch((err) => {
              console.error("Error al obtener empresas", err);
              return usuarioLogin;
            })
        }
      })
      .catch((err) => {
        console.error("Error al obtener empresas", err);
        return usuarioLogin;
      })
    return usuarioLogin;


  }

  addEstanciaNueva(empresa: Empresa, estancia: Estancias) {
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('mesas').doc(estancia.nombreEstancia).set(estancia);
  }

  addComanda(empresa: Empresa, comanda: Comanda){
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('comandas').doc(comanda.id.toString()).set(comanda);
  }

  addTicket(empresa: Empresa, ticket: Ticket){
    console.log(ticket);

    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('tickets').doc(ticket.id.toString()).set(ticket);
  }

  async getTicketsRestaurante(empresa: Empresa) {

    let tickets = this.firebaseFirestore.collection<Empresa>('empresas').doc(empresa.NIFEmpresa)
      .collection('tickets').snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        })
      )

    return tickets;
  }

  async getComandasRestaurante(empresa: Empresa) {

    let comandas = this.firebaseFirestore.collection<Empresa>('empresas').doc(empresa.NIFEmpresa)
      .collection('comandas').snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        })
      )

    return comandas;
  }

  async getEmpleadosEmpresa(empresa:Empresa){
    let empleados = this.firebaseFirestore.collection<Empresa>('empresas').doc(empresa.NIFEmpresa)
    .collection('empleados').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )

  return empleados;
  }

  addMesaEstancia(empresa: Empresa, estanciaNueva: Estancias) {
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('mesas').doc(estanciaNueva.nombreEstancia).set(estanciaNueva);
  }

  async getCartaRestaurante(empresa: Empresa) {
    return this.firebaseFirestore.collection<Empresa>('empresas').doc(empresa.NIFEmpresa).collection('carta').valueChanges();
  }

  async getEstanciasRestaurante(empresa: Empresa) {

    let estancias = this.firebaseFirestore.collection<Empresa>('empresas').doc(empresa.NIFEmpresa)
      .collection('mesas').snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        })
      )

    return estancias;
  }



  async addCategoriaCarta(empresa: Empresa, categoria: CategoriaPlatos) {
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('carta').doc(categoria.nombreCategoria).set(categoria);
  }

  modificarEmpleado(empresa:Empresa, usuario:Usuario){
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('empleados').doc(usuario.id).set(usuario);
  }

  deleteEmpleado(empresa:Empresa, usuario:Usuario){
    return this.firebaseFirestore.collection('empresas').doc(empresa.NIFEmpresa).collection('empleados').doc(usuario.id).delete();
  }
}
