import { Storage } from '@capacitor/storage';
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { VariablesService } from './variables.service';

@Injectable({
  providedIn: 'root'
})
export class ProcesosGlobalesService {
  keysEncriptadas: string[];

  constructor(
    public variables: VariablesService,
    ) { }

  async hayDatosAlmacenados() {

    await Storage.keys().then(res => this.keysEncriptadas = res.keys);
    if (this.keysEncriptadas.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  encriptar(varAEncriptar) {
    let varEncriptada = CryptoJS.AES.encrypt(varAEncriptar, this.variables.claveEncriptacion).toString();
    return varEncriptada;
  }

  /**
   * Desncripta la variable que se pasa por parámetro
   * @param varADesencriptar Variable a desencriptar
   * @returns Variable desencriptada
   */
   desencriptar(varADesencriptar) {
    let varDesencriptado = CryptoJS.AES.decrypt(varADesencriptar, this.variables.claveEncriptacion);
    varDesencriptado = varDesencriptado.toString(CryptoJS.enc.Utf8);
    return varDesencriptado;
  }

  async eliminarLocalStorage(key) {
    let keyBorrar: string = '';
    await Storage.keys().then(res => this.keysEncriptadas = res.keys);
    for (var clave of this.keysEncriptadas) {
      if (this.desencriptar(clave) == key) {
        keyBorrar = clave;
      }
    }
    await Storage.remove({ key: keyBorrar });

  }

  async guardarEnStorage(key: string, value: string) {
    // console.log("Guardando storage de: ", key, value);

    await this.eliminarLocalStorage(key);
    await Storage.set({
      key: this.encriptar(key),
      value: this.encriptar(value)
    });

    // console.log("Guardado en el storage");

  }

  async obtenerEnStorage(key: string) {
    // console.log("Obteniendo storage de: ", key);

    let keyEncriptada: string = '';
    if (await this.hayDatosAlmacenados()) {
      for (var clave of this.keysEncriptadas) {
        if (this.desencriptar(clave) == key) {
          keyEncriptada = clave;
        }
      }

      // console.log(keyEncriptada);


      if (keyEncriptada != '') {
        const res = await Storage.get({ key: keyEncriptada });
        let value = this.desencriptar(res.value);

        // console.log("Valor desencriptado",value);

        return  JSON.parse(value);
      } else return null;
    } else {
      console.log('No hay datos guardado en local storage');
    }
  }
}
