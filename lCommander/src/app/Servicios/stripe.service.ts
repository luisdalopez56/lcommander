import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  constructor(
    public http: HttpClient
  ) { }

  charge(cantidad, tokenId) {
    return this.http.post('https://europe-west2-lcommander-203bf.cloudfunctions.net/stripePagos/stripe_checkout', {
      stripeToken: tokenId,
      cantidad: cantidad
    },{}).toPromise();
  }
}
