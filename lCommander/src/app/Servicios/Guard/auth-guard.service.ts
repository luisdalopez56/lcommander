import { ControlInformacionService } from './../control-informacion.service';
import { VariablesService } from './../variables.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    public variables: VariablesService,
    public router: Router,
    public alertsService: ControlInformacionService
  ) { }

  canActivate() {
    // If the user is not logged in we'll send them back to the home page
    if (!this.variables.loginCorrecto) {
      console.log('No estás logueado');
      this.router.navigate(['/']);
      this.alertsService.mostrarToast("No estás logeado","danger");
      return false;
    }

    return true;
  }
}
