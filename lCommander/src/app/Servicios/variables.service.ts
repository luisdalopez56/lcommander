import { Usuario } from './../Modelos/Usuario';
import { Injectable } from '@angular/core';
import { Departamento } from '../Modelos/Departamento';
import { Empresa } from '../Modelos/Empresa';
import { CategoriaPlatos } from '../Modelos/CategoriaPlatos';
import { Comanda } from '../Modelos/Comanda';
import { Ticket } from '../Modelos/Ticket';

@Injectable({
  providedIn: 'root'
})
export class VariablesService {

  public loginCorrecto: boolean = false;
  public appPages;

  public empleadoSesion: Usuario;
  public departamentos: Departamento[];

  //PAGINAS PARA MENU SEGUN PERFIL
  public menuAdmin = [
    { title: 'Dashboard', url: '/dashboard', icon: 'grid' },
    { title: 'Mesas', url: '/mesas', src: "../assets/Iconos/restaurant-table-and-chairs-svgrepo-com.svg" },
    { title: 'Cocina', url: '/cocina', src: '../assets/Iconos/chef-svgrepo-com.svg' },
    { title: 'Comandas', url: '/comandas', src: '../assets/Iconos/menu-paper-svgrepo-com.svg' },
    { title: 'Carta', url: '/carta', src: '../assets/Iconos/menu-svgrepo-com.svg' },
    { title: 'Camareros', url: '/camareros', src: '../assets/Iconos/restaurant-covered-plate-on-a-hand-svgrepo-com.svg' },
    { title: 'Empleados', url: '/empleados', icon: 'people' },
    { title: 'Tickets', url: '/tickets', icon: 'reader' },

    // { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
    // { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
    // { title: 'Spam', url: '/folder/Spam', icon: 'warning' },
  ];

  public menuCamareros = [
    { title: 'Dashboard', url: '/dashboard', icon: 'grid' },
    { title: 'Camareros', url: '/camareros', src: '../assets/Iconos/restaurant-covered-plate-on-a-hand-svgrepo-com.svg' },
    { title: 'Mesas', url: '/mesas', src: "../assets/Iconos/restaurant-table-and-chairs-svgrepo-com.svg" },
    { title: 'Comandas', url: '/comandas', src: '../assets/Iconos/menu-paper-svgrepo-com.svg' },
    { title: 'Carta', url: '/carta', src: '../assets/Iconos/menu-svgrepo-com.svg' },
    { title: 'Tickets', url: '/tickets', icon: 'reader' },
  ]

  public menuCocina = [
    { title: 'Dashboard', url: '/dashboard', icon: 'grid' },
    { title: 'Cocina', url: '/cocina', src: '../assets/Iconos/chef-svgrepo-com.svg' },
    { title: 'Comandas', url: '/comandas', src: '../assets/Iconos/menu-paper-svgrepo-com.svg' },
    { title: 'Carta', url: '/carta', src: '../assets/Iconos/menu-svgrepo-com.svg' },
  ]

  empresaActual: Empresa;
  carta: CategoriaPlatos[];
  comandas: Comanda[];
  tickets: Ticket[];
  empleadosEmpresa: Usuario[];
  accesoCliente: boolean = false;
  claveEncriptacion: any ="lcommander";
  recordarUsuario:boolean
  tokenFCMDispositivo: any;
  constructor(
  ) {
    this.departamentos = [
      { id: 0, nombreDepartamento: "Administrador" },
      { id: 1, nombreDepartamento: "Cocina" },
      { id: 2, nombreDepartamento: "Camareros" }
    ];
this.empleadosEmpresa = [];


    this.appPages = [];



  };


}
