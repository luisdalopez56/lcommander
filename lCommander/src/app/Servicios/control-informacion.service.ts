import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ControlInformacionService {
public loadingObject;
  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) { }

  async mostrarToast(mensaje: string, color?: string, tiempo?: number) {

    if (!tiempo) tiempo = 4000;
    if (!color) color = "primary";

    let toast = await this.toastCtrl.create({
      color: color,
      duration: tiempo,
      message: mensaje
    })

    toast.present();

  }

  async presentLoading(mensaje: string){
    if(!this.loadingObject){
      this.loadingObject = await this.loadingCtrl.create({
        message:mensaje
      })

      await this.loadingObject.present();
    }
  }

  async dismissLoading(){
    if(this.loadingObject){
    await this.loadingCtrl.dismiss();
    this.loadingObject = null;
    }
  }
}
