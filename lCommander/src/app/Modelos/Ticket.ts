import { PlatoComanda } from './Comanda';
import { Mesa } from 'src/app/Modelos/Mesa';
import { Plato } from 'src/app/Modelos/Plato';
export class Ticket {
  id: string;
  platos: PlatoComanda[];
  estado: 0 | 1 | 2;
  precioTotal?: number;
  fechaTicket: string;
  mesa: Mesa
}
