import { Plato } from './Plato';
export class CategoriaPlatos{
  nombreCategoria: string;
  platos: Plato[];
}
