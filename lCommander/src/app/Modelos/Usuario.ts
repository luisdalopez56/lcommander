import { Departamento } from './Departamento';

export interface  TokenFCM {
  token: string;
  plataforma: string;
}
export class Usuario {
  id?: string;
  email: string;
  nombre: string;
  password: string;
  departamento: Departamento;
  listaToken: TokenFCM[]
}


