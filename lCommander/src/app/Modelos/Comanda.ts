import { Usuario } from './Usuario';
import { Estancias } from './Estancias';
import { Mesa } from './Mesa';

export interface PlatoComanda{
    id:string;
    nombrePlato: string;
    estado: 0|1|2|3;
    usuarioCocina?: Usuario;
    observaciones?: string;
    precio: number;
}

export class Comanda {
  id: string;
  fechaComanda: string;
  mesa: Mesa
  nombreEstancia: string;
  platos: PlatoComanda[];
  estado: 0 | 1;
}
