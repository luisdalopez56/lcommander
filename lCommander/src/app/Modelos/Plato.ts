export class Plato {
  id:string;
  nombrePlato: string;
  descripcion?: string;
  alergenos: string[];
  precio: number;
  imagenUrl?: string;
  nombreImagen?: string;
  disponible: boolean;
}
