import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './Servicios/Guard/auth-guard.service';

const routes: Routes = [

  {
    path: '',
    loadChildren: () => import('./Paginas/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./Paginas/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'mesas',
    loadChildren: () => import('./Paginas/mesas/mesas.module').then( m => m.MesasPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'comandas',
    loadChildren: () => import('./Paginas/comandas/comandas.module').then( m => m.ComandasPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'carta',
    loadChildren: () => import('./Paginas/carta/carta.module').then( m => m.CartaPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'cocina',
    loadChildren: () => import('./Paginas/cocina/cocina.module').then( m => m.CocinaPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'camareros',
    loadChildren: () => import('./Paginas/camareros/camareros.module').then( m => m.CamarerosPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'empleados',
    loadChildren: () => import('./Paginas/empleados/empleados.module').then( m => m.EmpleadosPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'tickets',
    loadChildren: () => import('./Paginas/tickets/tickets.module').then( m => m.TicketsPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'clientes',
    loadChildren: () => import('./Paginas/clientes/clientes.module').then( m => m.ClientesPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
