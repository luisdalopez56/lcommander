import { ProcesosGlobalesService } from './Servicios/procesos-globales.service';
import { FirebaseFirestoreService } from './Servicios/Firebase/firebase-firestore.service';
import { Router } from '@angular/router';
import { VariablesService } from './Servicios/variables.service';
import { GlobalConf } from './Configuracion/GlobalConf';
import { Component, OnInit } from '@angular/core';
import { initializeApp } from "firebase/app";
import { MenuController, Platform } from '@ionic/angular';
import { StatusBar, Style } from '@capacitor/status-bar';
import { CategoriaPlatos } from './Modelos/CategoriaPlatos';
import { Comanda } from './Modelos/Comanda';
import { Ticket } from './Modelos/Ticket';
import {loadStripe} from '@stripe/stripe-js';
import { LocalNotifications } from '@capacitor/local-notifications';
import { SwUpdate } from '@angular/service-worker';
import { Usuario } from './Modelos/Usuario';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  constructor(
    public variables: VariablesService,
    public platform: Platform,
    public router: Router,
    public menu: MenuController,
    private swUpdate: SwUpdate,
    public procesosGlobales:ProcesosGlobalesService,
    public firestoreService: FirebaseFirestoreService
  ) {
    LocalNotifications.requestPermissions();
  }
  async ngOnInit(): Promise<void> {
    if (this.platform.is('ios')) {
      await StatusBar.setStyle({ style: Style.Default });
      window.screen.orientation.lock('portrait');
    }
    if (this.platform.is('android')) {
      await StatusBar.setStyle({ style: Style.Light });
      window.screen.orientation.lock('portrait');
    }
    if (this.platform.is('desktop')) {
      console.log("Es un browser");
      const body = document.getElementsByTagName('ion-app')[0].style;
      body.margin = "0 20%";
    }
  }

  actualizarVersion() {
    if (!this.swUpdate.isEnabled) {
      console.log("SwUpdate not enbaled");
      return
    } else {
      console.log("SwUpdate enbaled");
    }
    this.swUpdate.checkForUpdate().then(() => console.log('Comprobada version PWA al abrir aplicacion'));
    this.swUpdate.available.subscribe(async (event) => {
      console.log('current: ', event.current, 'available: ', event.available)
      this.swUpdate.activateUpdate().then(() => location.reload())

      // if(confirm('Hay una nueva versión disponible. Acepte para actualizar')){
      //   this.swUpdate.activateUpdate().then(()=> location.reload())
      // }
    })
    // this.swUpdate.activated.subscribe((event)=>{
    //   console.log('current: ',event.previous, 'available: ',event.current)
    // })


  }

  async cerrarSesion(){

    await this.procesosGlobales.eliminarLocalStorage("user");
    await this.procesosGlobales.eliminarLocalStorage("pwd");
    await this.procesosGlobales.eliminarLocalStorage("recordar");

    this.menu.enable(false);
    this.router.navigateByUrl('');
    this.variables.loginCorrecto = false;
  }

  async cargarCarta() {
    (await this.firestoreService.getCartaRestaurante(this.variables.empresaActual)).subscribe((res) => {
      console.log("Carta cambiada: ", res);
      this.variables.carta = res as CategoriaPlatos[];
    })
  }

  async cargarEmpleados() {
    await (await this.firestoreService.getEmpleadosEmpresa(this.variables.empresaActual)).subscribe((usuarios) => {
      this.variables.empleadosEmpresa = usuarios as Usuario[];
    })
  }


  async cargarComandas() {
    (await this.firestoreService.getComandasRestaurante(this.variables.empresaActual)).subscribe((res) => {
      console.log("Comandas cambiada: ", res);
      this.variables.comandas = res as Comanda[];
    })
  }

  async cargarTickets() {
    (await this.firestoreService.getTicketsRestaurante(this.variables.empresaActual)).subscribe((res) => {
      console.log("Tickets cambiada: ", res);
      this.variables.tickets = res as Ticket[];
    })
  }
}
