importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyBavkJcDug2WPWajE80eguK6DBdp6nxWpU",
    authDomain: "lcommander-203bf.firebaseapp.com",
    databaseURL: "https://lcommander-203bf-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "lcommander-203bf",
    storageBucket: "lcommander-203bf.appspot.com",
    messagingSenderId: "900585027835",
    appId: "1:900585027835:web:8459ce1ef6908146a1585e"
});

const messaging = firebase.messaging();

//Aquí controlamos las notificaciones que le llegan a la PWA cuando está en segundo plano
messaging.onBackgroundMessage((payload) => {
    console.log("Notificacion entrante", payload);
    let notificacion = JSON.parse(payload.data.notification)
    const notificationTitle = notificacion.title;

    const notificationOptions = {
        body: notificacion.body,
        icon: './assets/Icono.png',
    }
    self.registration.showNotification(notificationTitle, notificationOptions);
});