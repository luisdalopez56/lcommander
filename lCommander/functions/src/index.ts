import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const express = require('express');
const cors = require('cors')({ origin: true });
const stripe = require('stripe')
  ('sk_test_51KOJ6WKp38wBwz6fdI65CFgOog4a4ust8W6cKMsYOkWzAaeFH4MPwPz8BwUgAPYahJW1QgcVOpQlkvRcfmiWdQx100rzUlvLIP');
const stripePagos = express();
stripePagos.use(cors);

admin.initializeApp(functions.config().firebase);

stripePagos.post('/stripe_checkout', async (req: any, res: any) => {
  return cors(req, res, async () => {
    console.log(req.body);

    let stripeToken = req.body.stripeToken;
    let cantidad = req.body.cantidad;
    console.log(stripeToken, cantidad);


    const cantInEur = Math.round(cantidad * 100);

    const chargeObject = await stripe.charges.create(
      {
        amount: cantInEur,
        currency: 'eur',
        source: stripeToken,
        capture: false,
        description: "Lcommander, gestión de restaurantes"
      }
    );

    try {
      await stripe.charges.capture(chargeObject.id);
      res.status(200).send(chargeObject)
    } catch (error) {
      await stripe.refunds.create({charge: chargeObject});
    }



  })
})

exports.stripePagos = functions.region('europe-west2').https.onRequest(stripePagos);

